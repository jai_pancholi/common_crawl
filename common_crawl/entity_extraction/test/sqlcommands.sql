
select * from companies.companies_house_pdf_parsed limit 10

select * from companies.companies_house_pdf_parsed order by "AverageNumberEmployeesDuringPeriod" desc


create schema common_crawl
create table common_crawl.entity_extract (
id bigserial,
entity text,
entity_type text,
article_url text,
offset_byte int,
byte_length int,
warc_uri text
)

select * from common_crawl.entity_extract
select * from common_crawl.entity_extract where entity_type = 'ORGANIZATION'
select count(1) from common_crawl.entity_extract 
select count(1) from common_crawl.entity_extract where entity_type = 'ORGANIZATION'
select distinct article_url from common_crawl.entity_extract
truncate common_crawl.entity_extract

select article_url, entity from common_crawl.entity_extract where entity_type = 'ORGANIZATION'
select article_url, array_agg(entity) from common_crawl.entity_extract where entity_type = 'ORGANIZATION' group by 1

create table common_crawl.entity_articles_scraped (
id bigserial,
article_url text,
outbound_link text,
anchor_text text
)

select * from common_crawl.entity_articles_scraped
select count(1) from common_crawl.entity_articles_scraped

truncate common_crawl.entity_articles_scraped


SELECT DISTINCT cc_ee.article_url, cc_ee.offset_byte, cc_ee.byte_length, cc_ee.warc_uri, cc_eac.article_url from common_crawl.entity_extract cc_ee left join common_crawl.entity_articles_scraped cc_eac on cc_eac.article_url = cc_ee.article_url where cc_ee.entity_type = 'ORGANIZATION' and cc_eac.article_url is null 



select entity, article_url from common_crawl.entity_extract where entity_type = 'ORGANIZATION'
select article_url, anchor_text, outbound_link from common_crawl.entity_articles_scraped
select count(1) from common_crawl.entity_extract

select count(distinct entity) from common_crawl.entity_extract
 took l
select count(distinct article_url) from common_crawl.entity_articles_scraped


with tmp as (select
		ee.entity
		,ee.article_url
		,eas.anchor_text
		,eas.outbound_link
from common_crawl.entity_extract ee
join common_crawl.entity_articles_scraped eas on eas.article_url = ee.article_url
where ee.entity_type = 'ORGANIZATION')
--select *
--select count(1)
select count(distinct outbound_link)
from tmp
where (tmp.anchor_text ilike '%' || tmp.entity || '%') and (tmp.outbound_link !~* '.*(instagram|facebook|twitter|reuters|javascript|linkedin|youtube|amazon.com|google.com).*')



--filter out specific entities
--filter out specific domains (news, facebook, instagram)
--filter our speicifc anchor texts (privacy policy)

select * from companies.yell_clubs limit 10
select count(distinct yell_page) from companies.yell_clubs limit 10


CREATE DATABASE airflow_celery;
GRANT ALL PRIVILEGES ON DATABASE airflow_celery to airflow;
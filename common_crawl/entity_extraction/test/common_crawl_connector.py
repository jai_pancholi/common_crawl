import re
import requests
import io
import gzip
import logging

log = logging.getLogger('app.collector')

class CommonCrawlConnector:
    def HTML_from_CC(self, uri, byte_offset, length):
        """ Gets HTML from Common crawl

        Args:
            uri: S3 WARC file
            byte_offset: offset of where the HTML is in WARC
            length: byte length of html record

        Returns
            response: Full HTML from article

        """
        byte_offset = int(byte_offset)
        length = int(length)

        offset_end = byte_offset + length - 1

        file_name = re.search("(crawl-data.*)", uri).group(1)

        # Get the file via HTTPS, no need to worry about S3 credentials
        prefix = "https://commoncrawl.s3.amazonaws.com/"

        # We can use the Range header to ask for just this set of bytes
        resp = requests.get(
            prefix + file_name,
            headers={"Range": "bytes={}-{}".format(byte_offset, offset_end)},
        )

        raw_data = io.BytesIO(resp.content)
        f = gzip.GzipFile(fileobj=raw_data)

        # We have the WARC response, formmated:
        data = f.read()
        try:
            data = str(data, 'utf-8')
        except Exception as err:
            data = str(data, "latin-1")

        response = ""

        if len(data):
            try:
                warc, header, response = data.strip().split("\r\n\r\n", 2)
            except Exception as err:
                log.error(err)

        return response
        # return header, response

    def HTMLs_from_CC(self, articles):
        """ Iterates through matched articles and gets full HTML from CC

        Args:
            articles: Matched articles

        Returns
            response: Full HTML from articles

        """

        log.info("Pulling data from Common Crawl")
        for article in articles:
            uri = article["uri"]
            byte_offset = article["offset"]
            length = article["length"]
            article["html"] = self.HTML_from_CC(uri, byte_offset, length)

        log.info("HTMLs obtained from Common Crawl")

        return articles
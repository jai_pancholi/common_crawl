from warcio.archiveiterator import ArchiveIterator
import os

from nltk.tag import StanfordNERTagger
from nltk.tokenize import word_tokenize

import time

from bs4 import BeautifulSoup
from newsplease import NewsPlease

from pprint import pprint

import psycopg2, psycopg2.extras

uri = '/Users/jai/Sites/common_crawl/entity_extraction/test/crawl-data/CC-NEWS/2018/07/split/CC-NEWS-20180705072929-00806.warc.gz-aa'

base_dir = os.path.abspath(os.path.dirname(__file__))
uri = os.path.join(base_dir, uri)

data = []

st = StanfordNERTagger('./cc-pyspark/cc-pyspark/models/stanford-ner-2018-02-27/classifiers/english.all.3class.distsim.crf.ser.gz',
                        './cc-pyspark/cc-pyspark/models/stanford-ner-2018-02-27/stanford-ner.jar',
                        encoding='utf-8')

def combine_sequential_tokens(classified_text):
    new_tokens = []

    for token in classified_text:
        if len(new_tokens) > 0 and (token[1] == new_tokens[-1][1]):
            new_tokens[-1][0] = new_tokens[-1][0] + ' ' + token[0]
        else:
            new_tokens.append(list(token))

    return new_tokens

def process_record(record, st, uri, archive_iterator):
    if record.rec_type == 'response':
        # WARC response record

        content_type = record.http_headers.get_header('content-type', None)
        if not(content_type is None or 'html' not in content_type):
            # skip non-HTML or unknown content types

            data = record.content_stream().read()
            try:
                data = data.decode('utf-8')
            except:
                data = data.decode('latin-1')

            try:

                np = NewsPlease.from_html(data)

                if np.text:
                    if np.language == 'en':
                        tokenized_text = word_tokenize(np.text)
                        classified_text = st.tag(tokenized_text)
                        classified_text = combine_sequential_tokens(classified_text)

                        url = record.rec_headers.get_header('WARC-Target-URI')
                        offset = archive_iterator.get_record_offset()
                        length = archive_iterator.get_record_length()

                        for token in classified_text:
                            if token[1] != 'O':
                                yield (token[0], token[1], url, offset, length, uri)
            except Exception as ex:
                print(ex)

def insert_rows(SQL, data):
	# Insert data into database
	conn = psycopg2.connect(
		database=os.environ.get('DATABASE_NAME'),
		user=os.environ.get('USER'),
		host=os.environ.get('DATABASE_HOST'),
		password=os.environ.get('DATABASE_PASSWORD')
	)
	cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
	cursor.execute(SQL, data)
	conn.commit()
	conn.close()

def run_job(stream=None):
    archive_iterator = ArchiveIterator(stream)

    for i, record in enumerate(archive_iterator):
        # NEEDS TO BE PLACED AFTER HTML EXTRACTION FOR SOME REASON
        # offset = archive_iterator.get_record_offset()
        # length = archive_iterator.get_record_length()
        print(i)
        if i < 885:
            continue

        for res in process_record(record, st, uri, archive_iterator):
            # data.append(res)
            if res:
                insert_rows(SQL, res)



SQL = """
INSERT INTO common_crawl.entity_extract (entity, entity_type, article_url, offset_byte, byte_length, warc_uri)
VALUES (%s,%s,%s,%s,%s,%s)
"""

uris = ['/Users/jai/Sites/common_crawl/entity_extraction/test/crawl-data/CC-NEWS/2018/07/CC-NEWS-20180705072929-00806.warc.gz']

start = time.time()

for uri in uris:
    stream = open(uri, 'rb')

    run_job(stream)
    
    
end = time.time()
print(end - start)


# -*- coding: utf-8 -*-

from nltk.tag import StanfordNERTagger
from nltk.tokenize import word_tokenize

import requests

url = 'https://www.insurancetimes.co.uk/andy-homer-among-digital-fineprint-backers-in-2m-funding-round/1425963.article'
text = requests.get(url).text

def combine_sequential_tokens(classified_text):
    new_tokens = []

    for token in classified_text:
        if len(new_tokens) > 0 and (token[1] == new_tokens[-1][1]):
            new_tokens[-1][0] = new_tokens[-1][0] + ' ' + token[0]
        else:
            new_tokens.append(list(token))


    return new_tokens

st = StanfordNERTagger('./models/stanford-ner-2018-02-27/classifiers/english.all.3class.distsim.crf.ser.gz',
					   './models/stanford-ner-2018-02-27/stanford-ner.jar',
					   encoding='utf-8')


tokenized_text = word_tokenize(text)
classified_text = st.tag(tokenized_text)

classified_text = combine_sequential_tokens(classified_text)

for token in classified_text:
    if token[1] != 'O':
        print(token)
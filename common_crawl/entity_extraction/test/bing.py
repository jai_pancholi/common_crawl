import psycopg2, psycopg2.extras
from common_crawl_connector import CommonCrawlConnector
from bs4 import BeautifulSoup, SoupStrainer
import os

import pandas as pd
from pprint import pprint

from tld import get_tld, get_fld


def pull_urls(limit=10):
    SQL = """SELECT DISTINCT entity, article_url, offset_byte, byte_length, warc_uri from common_crawl.entity_extract where entity_type = 'ORGANIZATION' limit {}""".format(limit)

    conn = psycopg2.connect(
        database=os.environ.get("DATABASE_NAME"),
        user=os.environ.get("USER"),
        host=os.environ.get("DATABASE_HOST"),
        password=os.environ.get("DATABASE_PASSWORD"),
    )
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(SQL)
    data = cursor.fetchall()
    conn.close()

    return data

data = pull_urls()
pprint(data)
import psycopg2, psycopg2.extras
from common_crawl_connector import CommonCrawlConnector
from bs4 import BeautifulSoup, SoupStrainer
import os

import pandas as pd
from pprint import pprint

from tld import get_tld, get_fld


def pull_urls(limit=10):
    # SQL = """select article_url, entity from common_crawl.entity_extract where entity_type = 'ORGANIZATION' limit {}""".format(limit)
    # SQL = """select article_url, array_agg(entity) from common_crawl.entity_extract where entity_type = 'ORGANIZATION' group by 1 limit {}""".format(limit)
    # SQL = """SELECT DISTINCT article_url, offset_byte, byte_length, warc_uri from common_crawl.entity_extract where entity_type = 'ORGANIZATION' limit {}""".format(limit)
    SQL = """SELECT DISTINCT cc_ee.article_url, cc_ee.offset_byte, cc_ee.byte_length, cc_ee.warc_uri from common_crawl.entity_extract cc_ee left join common_crawl.entity_articles_scraped cc_eac on cc_eac.article_url = cc_ee.article_url where cc_ee.entity_type = 'ORGANIZATION' and cc_eac.article_url is null limit {}""".format(
        limit
    )

    conn = psycopg2.connect(
        database=os.environ.get("DATABASE_NAME"),
        user=os.environ.get("USER"),
        host=os.environ.get("DATABASE_HOST"),
        password=os.environ.get("DATABASE_PASSWORD"),
    )
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor.execute(SQL)
    data = cursor.fetchall()
    conn.close()

    return data


def insert_rows(SQL, data):
    # Insert data into database
    conn = psycopg2.connect(
        database=os.environ.get("DATABASE_NAME"),
        user=os.environ.get("USER"),
        host=os.environ.get("DATABASE_HOST"),
        password=os.environ.get("DATABASE_PASSWORD"),
    )
    # cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cursor = conn.cursor()
    cursor.execute(SQL, data)
    conn.commit()
    conn.close()


def is_outbound_link(source_href, target_href):
    source_domain = get_fld(source_href, fix_protocol=True, fail_silently=True)
    target_domain = get_fld(target_href, fix_protocol=True, fail_silently=True)

    if (
        target_href.startswith("/")
        or target_href.startswith("#")
        or target_href.startswith("mailto:")
    ):
        return False
    elif source_domain != target_domain:
        return True
    else:
        return False


cc = CommonCrawlConnector()

data = pull_urls(limit=10000)
for i, article in enumerate(data):
    article_url = article["article_url"]
    warc_uri = article["warc_uri"]
    byte_length = article["byte_length"]
    byte_offset = article["offset_byte"]

    html = cc.HTML_from_CC(warc_uri, byte_offset, byte_length)

    parse_only = SoupStrainer(["a"])
    soup = BeautifulSoup(html, "lxml", parse_only=parse_only)

    links = soup.findAll("a")

    for link in links:
        if link.has_attr("href") and link.text is not None and link.text != "":
            if is_outbound_link(article_url, link["href"].strip()):
                SQL = """
					INSERT INTO common_crawl.entity_articles_scraped (article_url, outbound_link, anchor_text)
					VALUES (%s, %s, %s)
				"""
                data = [article_url, link["href"].strip(), link.text.strip().replace("\n", " ")]
                insert_rows(SQL, data)

# print(df)
# df = pd.DataFrame(data)


# LOOK IN AIRFLOW DAGS FOR UPDATEs
# import os
# import re
# import boto3
# from boto.s3.key import Key
# from pprint import pprint
# import sys

# class InputDataPipeline():
#     def __init__(self):
#         self.bucket_name = 'common-crawl-insights'
#         self.directory_name_month = 'spark/input/production/monthly/'
#         self.directory_name_weekly = 'spark/input/production/weekly/'

#     def download_files(self):
#         self.raw_files = os.popen('aws s3 ls --recursive s3://commoncrawl/crawl-data/CC-NEWS/').read().strip().split('\n')

#     def parse_files(self):
#         self.parsed_files = []
#         for file in self.raw_files:
#             self.parsed_files.append('s3://commoncrawl/'+re.search('(crawl-data.*)', file).group(1))

#     def split_files_monthly(self):
#         self.files_split_by_month = {}
#         for file in self.parsed_files:
#             matches = re.search('crawl-data/CC-NEWS/(.*?)/(.*?)/', file)
#             year = matches.group(1)
#             month = matches.group(2)

#             if year not in self.files_split_by_month.keys():
#                 self.files_split_by_month[year] = {}
            
#             if month not in self.files_split_by_month[year].keys():
#                 self.files_split_by_month[year][month] = []

#             self.files_split_by_month[year][month].append(file)

#     def upload_files_monthly(self):
#         s3 = boto3.client('s3')

#         for year, months in self.files_split_by_month.items():
#             for month, files in months.items():
#                 file_as_string = '\n'.join(file for file in files)
#                 sub_directory_name = year + '/' + month + '.txt'
#                 s3.put_object(Bucket=self.bucket_name, Key=(self.directory_name_month+sub_directory_name), Body=file_as_string)

#     @staticmethod
#     def day_to_week(day):
#         """Returns a number between 1 and 5 to represent week of the month
#         """
#         week = (int(day) / 7)+1
#         # print(week)
#         return week

#     def split_files_weekly(self):
#         self.split_files_monthly()
        
#         self.files_split_by_week = {}

#         num = 00

#         for year, months in self.files_split_by_month.items():
#             for month, files in months.items():
#                 for file in files:
#                     day = re.search('CC-NEWS-\d{6}(\d{2})', file).group(1)
#                     week = self.day_to_week(day)

#                     if year not in self.files_split_by_week.keys():
#                         self.files_split_by_week[year] = {}
        
#                     if month not in self.files_split_by_week[year].keys():
#                         self.files_split_by_week[year][month] = {}

#                     if week not in self.files_split_by_week[year][month].keys():
#                         self.files_split_by_week[year][month][week] = []

#                     self.files_split_by_week[year][month][week].append(file)

#     def upload_files_weekly(self):
#             s3 = boto3.client('s3')

#             for year, months in self.files_split_by_week.items():
#                 for month, weeks in months.items():
#                     for week, files in weeks.items():
#                         file_as_string = '\n'.join(file for file in files)
#                         sub_directory_name = year + '/' + month + '/' + str(week) + '.txt'
#                         s3.put_object(Bucket=self.bucket_name, Key=(self.directory_name_weekly+sub_directory_name), Body=file_as_string)
#                         # print(sub_directory_name)
#                         # sys.exit()

# if __name__ == '__main__':
#     pipe = InputDataPipeline()
#     pipe.download_files()
#     pipe.parse_files()
#     # pipe.split_files_weekly()
#     # pipe.upload_files_weekly()
#     pipe.split_files_monthly()
#     pipe.upload_files_monthly()
    
#     # days = [
#     #     'Mon',
#     #     'Tue',
#     #     'Wed',
#     #     'Thu',
#     #     'Fri',
#     #     'Sat',
#     #     'Sun'
#     # ]
#     # for i in range(30):
#     #     week = InputDataPipeline.day_to_week(i)
#     #     print(week, days[i%7])
        
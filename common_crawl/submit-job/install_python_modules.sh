# !/bin/bash -xe

# Non-standard and non-Amazon Machine Image Python modules:
# sudo pip install -U \
#   awscli            \
#   boto              \
#   ciso8601          \
#   ujson             \
#   botocore          \
#   boto3             \
#   ujson             \
#   warcio            \
#   Pillow==5.0.0\
#   https://s3-us-west-2.amazonaws.com/jdimatteo-personal-public-readaccess/nltk-2.0.5-https-distribute.tar.gz\
#   news-please

sudo python3 -m pip install cherrypy \
  boto \
  awscli \
  ujson   \
  botocore  \
  boto3   \
  ujson   \
  warcio  \
  news-please
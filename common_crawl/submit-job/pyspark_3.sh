# !/bin/bash -xe
while [ ! -f /etc/spark/conf/spark-env.sh ]
do
    sleep 1
    echo waiting
done
sudo sed -i -e '$a\export PYSPARK_PYTHON=/usr/bin/python3' /etc/spark/conf/spark-env.sh
exit 0
EOF
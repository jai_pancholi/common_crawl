# Instructions

## S3 Bucket Setup
We need to have all of following files uploaded to an S3 bucket:

* ```install_python_modules.sh``` is a bash script that will install all neccesary python packages in the cluster.

* ```maximize-spark-default-config.sh``` will configure optimal Spark settings by modifying spark-default.conf.

* ```job_for_production_emr.py``` is the script that will take the location of Common Crawl News Warcs, download and parse them into a parquet file in the HDFS.

* ```es_spark_write.py``` is the script that sends the data stored in the HDFS into the Elasticsearch index.

* ```elasticsearch-hadoop-6.3.1.jar``` is needed to facilitate the transfer of data from HDFS to ES.


## Run Locally
```bash
spark-submit cc-pyspark/job_for_production_emr_news_please.py --num_output_partitions 1 --log_level WARN ./cc-pyspark/input/test_warc.txt
```

## Find input files
Running ```download_split_warc.py``` will download the location for all Common Crawl News WARCs, split them by week, and upload them to an [S3 bucket](https://s3.console.aws.amazon.com/s3/buckets/common-crawl-insights/spark/input/production/?region=us-east-1&tab=overview).

## Download, parse files and send data to Elastic Search
Now that we have the location of the newest News files, we can set up a Spark job that will download, parse the files using NewsPlease and send the data to an Elasticsearch database. We can start these jobs in two ways, using a preconfigured Python script file, or using AWS CLI


### Preconfigured Script
Running submit_job.py like this
```sh
python submit_job.py s3://common-crawl-insights/spark/input/production/weekly/2016/12/1.txt
```

### AWS CLI
This command requires you to have preinstalled AWS CLI since the job will run on a Hadoop cluster using the AWS EMR service.

An example command that will download the first week of December 2018 files is:
```aws emr create-cluster --termination-protected --applications Name=Hadoop Name=Spark --ec2-attributes '{"KeyName":"jai_common_crawl","InstanceProfile":"EMR_EC2_DefaultRole","SubnetId":"subnet-40795d1d","EmrManagedSlaveSecurityGroup":"sg-ae96a6e5","EmrManagedMasterSecurityGroup":"sg-68a39323"}' --release-label emr-5.16.0 --log-uri 's3n://aws-logs-104207875720-us-east-1/elasticmapreduce/' --steps '[{"Args":["spark-submit","--deploy-mode","cluster","--master","yarn","s3:\/\/common-crawl-insights\/spark\/scripts\/job_for_production_emr_news_please.py","--num_output_partitions","100","s3:\/\/common-crawl-insights\/spark\/input\/production\/weekly\/2018\/12/1.txt","output_table"],"Type":"CUSTOM_JAR","ActionOnFailure":"CONTINUE","Jar":"command-runner.jar","Properties":"","Name":"Sparkapplication"},{"Args":["spark-submit","--deploy-mode","cluster","--master","yarn","--jars","s3:\/\/common-crawl-insights\/elasticsearch-hadoop-6.3.1.jar","s3:\/\/common-crawl-insights\/spark\/scripts\/es_spark_write.py","--data_location","\/user\/spark\/warehouse\/output_table","--index_type","news\/article"],"Type":"CUSTOM_JAR","ActionOnFailure":"CONTINUE","Jar":"command-runner.jar","Properties":"","Name":"Sparkapplication"}]' --instance-groups '[{"InstanceCount":10,"BidPrice":"0.07","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":32,"VolumeType":"gp2"},"VolumesPerInstance":1}]},"InstanceGroupType":"CORE","InstanceType":"m4.xlarge","Name":"Core - 2"},{"InstanceCount":1,"BidPrice":"0.07","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":32,"VolumeType":"gp2"},"VolumesPerInstance":1}]},"InstanceGroupType":"MASTER","InstanceType":"m4.xlarge","Name":"Master - 1"}]' --auto-terminate --auto-scaling-role EMR_AutoScaling_DefaultRole --bootstrap-actions '[{"Path":"s3://common-crawl-insights/install_python_modules.sh","Name":"Custom action"},{"Path":"s3://common-crawl-insights/maximize-spark-default-config.sh","Name":"Custom action"}]' --ebs-root-volume-size 10 --service-role EMR_DefaultRole --enable-debugging --name 'common_crawl' --scale-down-behavior TERMINATE_AT_TASK_COMPLETION --region us-east-1```

You can launch a persistent cluster to develop on with:
```aws emr create-cluster --termination-protected --applications Name=Hadoop Name=Spark --ec2-attributes '{"KeyName":"jai_common_crawl","InstanceProfile":"EMR_EC2_DefaultRole","SubnetId":"subnet-40795d1d","EmrManagedSlaveSecurityGroup":"sg-ae96a6e5","EmrManagedMasterSecurityGroup":"sg-68a39323"}' --release-label emr-5.16.0 --log-uri 's3n://aws-logs-104207875720-us-east-1/elasticmapreduce/' --instance-groups '[{"InstanceCount":2,"BidPrice":"0.07","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":32,"VolumeType":"gp2"},"VolumesPerInstance":1}]},"InstanceGroupType":"CORE","InstanceType":"m4.xlarge","Name":"Core - 2"},{"InstanceCount":1,"BidPrice":"0.07","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":32,"VolumeType":"gp2"},"VolumesPerInstance":1}]},"InstanceGroupType":"MASTER","InstanceType":"m4.xlarge","Name":"Master - 1"}]' --no-auto-terminate --auto-scaling-role EMR_AutoScaling_DefaultRole --bootstrap-actions '[{"Path":"s3://common-crawl-insights/install_python_modules.sh","Name":"Custom action"},{"Path":"s3://common-crawl-insights/maximize-spark-default-config.sh","Name":"Custom action"}]' --ebs-root-volume-size 10 --service-role EMR_DefaultRole --enable-debugging --name 'common_crawl' --region us-east-1```

You can add individual steps to a running cluster, specifiying the correct cluster id, with:
```aws emr add-steps --cluster-id j-1CBL5NJP6ASTB --steps '[{"Args":["spark-submit","--deploy-mode","cluster","--master","yarn","s3:\/\/common-crawl-insights\/spark\/scripts\/job_for_production_emr_news_please.py","--num_output_partitions","100","s3:\/\/common-crawl-insights\/spark\/input\/production\/weekly\/2016\/12/1.txt","output_table_two"],"Type":"CUSTOM_JAR","ActionOnFailure":"CONTINUE","Jar":"command-runner.jar","Properties":"","Name":"Sparkapplication"}]'```

```aws emr add-steps --cluster-id j-257ILEHRGHALJ --steps '[{"Args":["spark-submit","--deploy-mode","cluster","--master","yarn","s3:\/\/common-crawl-insights\/spark\/scripts\/test_local.py","--num_output_partitions","100","s3:\/\/common-crawl-insights\/spark\/input\/production\/weekly\/2016\/12/1.txt","output_table"],"Type":"CUSTOM_JAR","ActionOnFailure":"CONTINUE","Jar":"command-runner.jar","Properties":"","Name":"Sparkapplication"}]'```
or 
```aws emr add-steps --cluster-id j-1CBL5NJP6ASTB --steps '[{"Args":["spark-submit","--deploy-mode","cluster","--master","yarn","--jars","s3:\/\/common-crawl-insights\/elasticsearch-hadoop-6.3.1.jar","s3:\/\/common-crawl-insights\/spark\/scripts\/es_spark_write.py","--data_location","\/user\/spark\/warehouse\/output_table_two","--index_type","news-please\/article_two"],"Type":"CUSTOM_JAR","ActionOnFailure":"CONTINUE","Jar":"command-runner.jar","Properties":"","Name":"Sparkapplication"}]'```


## Interactive Debugging
The above command stores the output in the HDFS at the path. SSH into the master node and use ```hadoop fs -ls -R / | grep "^d"``` to list all the folders in the HDFS. You should see a folder called ```output_table```. You can load this into a spark dataframe by stepping into a spark shell with ```pyspark``` and running ```df = sqlContext.read.parquet('/user/spark/warehouse/output_table_two')```.
<!-- The above command stores the output in the HDFS at the path. SSH into the master node and use ```hadoop fs -ls -R / | grep "^d"``` to list all the folders in the HDFS. You should see a folder called ```output_table```. You can load this into a spark dataframe by stepping into a spark shell with ```pyspark``` and running ```df = sqlContext.read.parquet('/home/hadoop/warehouse')```. -->


## Airflow Usage
HOW?
import argparse
import time

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext

class WriteToES():
    name = 'WriteToES'

    def parse_arguments(self):
        """ Returns the parsed arguments from the command line """

        description = self.name
        if self.__doc__ is not None:
            description += " - "
            description += self.__doc__
        arg_parser = argparse.ArgumentParser(description=description)

        # arg_parser.add_argument("--es_node", default='https://search-search-news-sfpkikpael62xbbzy5kppregoy.us-east-1.es.amazonaws.com',
        # arg_parser.add_argument("--es_node", default='https://search-search-news-sfpkikpael62xbbzy5kppregoy.us-east-1.es.amazonaws.com/',
        #                         help="URL to ES node") 
        arg_parser.add_argument("--es_node", default='https://search-news-please-n7ccbdt2f2dbh4uk5veniliiaq.us-east-1.es.amazonaws.com/',
                                help="URL to ES node") 
        arg_parser.add_argument("--data_location", default=None,
                                help="filepath of parent of parquet file")
        arg_parser.add_argument("--index_type", default=None,
                                help="index and type of data to insert into ES")

        args = arg_parser.parse_args()

        return args


    def run(self):
        self.args = self.parse_arguments()

        print(self.args.data_location)

        conf = SparkConf()
        conf.set('spark.app.name', 'WriteToES')
        conf.set('es.nodes', self.args.es_node)
        conf.set('es.port', '443')
        # conf.set('es.nodes.client.only', 'true')

        sc = SparkContext(conf=conf)
        sqlContext = SQLContext(sc)
        df = sqlContext.read.parquet(self.args.data_location)

        # es_conf = {
        #     "es.resource": "index/type"
        # }

        # AWS
        df.write.format("org.elasticsearch.spark.sql")\
        .option('es.resource', self.args.index_type)\
        .option('es.nodes.wan.only', 'true')\
        .mode('append')\
        .save()

        # Local
        # df.write.format("org.elasticsearch.spark.sql")\
        # .option('es.resource', self.args.index_type)\
        # .save()

        sc.stop()

if __name__ == "__main__":
    start = time.time()
    job = WriteToES()
    job.run()
    end = time.time()
    print(end - start)

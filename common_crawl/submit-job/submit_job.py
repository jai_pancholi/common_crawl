# This script takes in a WARC Input file as an S3 path, and launches an EMR job that 
# runs news please on the pages in the WARC file and pipes the output to an Elastic Search
# Index.

import sys, re, argparse
import boto3
from pprint import pprint

"""Initialise cluster and step configuration variables. """
S3_LOG_URI = 's3n://aws-logs-104207875720-us-east-1/elasticmapreduce/'
MASTER_INSTANCE_TYPE = 'm4.xlarge'
SLAVE_INSTANCE_TYPE = 'm4.xlarge'
NUM_OF_INSTANCES = 2
BID_PRICE = '0.07'
EC2_KEY_NAME = 'jai_common_crawl'
AVAILABILITY_ZONE = 'us-east-1d'
SUBNET_ID = 'subnet-40795d1d'
SLAVE_SECURITY_GROUP = 'sg-ae96a6e5'
MASTER_SECURITY_GROUP = 'sg-68a39323'
SERVICE_ROLE = 'EMR_DefaultRole'
JOB_FLOW_ROLE = 'EMR_EC2_DefaultRole'
INSTALL_PYTHON_MODULES_SCRIPT_LOC = 's3://common-crawl-insights/install_python_modules.sh'
OPTIMISE_CLUSTER_SCRIPT_LOC = 's3://common-crawl-insights/maximize-spark-default-config.sh'
NEWS_PLEASE_PARSE_SCRIPT = 's3://common-crawl-insights/spark/scripts/job_for_production_emr_news_please.py'
ELASTIC_SEARCH_HADOOP_JAR = 's3://common-crawl-insights/elasticsearch-hadoop-6.3.1.jar'
ELASTIC_SEARCH_SCRIPT = 's3://common-crawl-insights/spark/scripts/es_spark_write.py'
ELASTIC_SEARCH_INDEX_TYPE = 'news-please/article_two'

def list_running_clusters(client):
    """
    List all of the running EMR cluster in AWS.
    """
    c = client.list_clusters()
    running_clusters = {'idle': [], 'busy': []}
    for cluster in c['Clusters']:
        if cluster['Status']['State'] == 'RUNNING' and cluster['Status']['StateChangeReason']['Message'] == 'Running step':
            running_clusters['busy'].append(cluster['Id'])
        elif cluster['Status']['State'] == 'RUNNING' or cluster['Status']['State'] == 'WAITING':
            running_clusters['idle'].append(cluster['Id'])

    return running_clusters

def choose_a_running_cluster(running_clusters):
    """
    Return an ID of running EMR clusters, with a preference for idle clusters. Returns None if no runnign clusters
    """
    # return an id of a running cluster, with a preference for idle clusters
    if len(running_clusters['idle']) > 0:
        return running_clusters['idle'][0]
    elif len(running_clusters['busy']) > 0:
        return running_clusters['busy'][0]
    else:
        return None

def create_cluster(client):
    """
    Create an AWS EMR cluster that will run our jobs. 
    Note, there is a vital bootstrap scripts that installs non native scripts.
    """
    response = client.run_job_flow(
        Name='NewsPlease',
        LogUri=S3_LOG_URI,
        Instances={
            'InstanceGroups': [
                {
                    'Name': 'Master',
                    'Market': 'SPOT',
                    'InstanceRole': 'MASTER',
                    'BidPrice': BID_PRICE,
                    'InstanceType': MASTER_INSTANCE_TYPE,
                    'InstanceCount': 1,
                    'Configurations': [],
                    'EbsConfiguration': {
                        'EbsBlockDeviceConfigs': [
                            {
                                'VolumeSpecification': {
                                    'VolumeType': 'gp2',
                                    'SizeInGB': 32
                                },
                                'VolumesPerInstance': 1
                            },
                        ],
                    },
                }, {
                    'Name': 'Worker',
                    'Market': 'SPOT',
                    'InstanceRole': 'CORE', #'TASK',
                    'BidPrice': BID_PRICE,
                    'InstanceType': SLAVE_INSTANCE_TYPE,
                    'InstanceCount': NUM_OF_INSTANCES - 1,
                    'Configurations': [],
                    'EbsConfiguration': {
                        'EbsBlockDeviceConfigs': [
                            {
                                'VolumeSpecification': {
                                    'VolumeType': 'gp2',
                                    'SizeInGB': 32
                                },
                                'VolumesPerInstance': 1
                            },
                        ],
                    },
                },
            ],
            'Ec2KeyName': EC2_KEY_NAME,
            'Placement': {
                'AvailabilityZone': AVAILABILITY_ZONE,
            },
            'KeepJobFlowAliveWhenNoSteps': False,
            'TerminationProtected': False,
            'EmrManagedMasterSecurityGroup': MASTER_SECURITY_GROUP,
            'EmrManagedSlaveSecurityGroup': SLAVE_SECURITY_GROUP,
        },
        BootstrapActions=[
            {
                'Name': 'Install Python Modules',
                'ScriptBootstrapAction': {
                    'Path': INSTALL_PYTHON_MODULES_SCRIPT_LOC,
                }
            },
            {
                'Name': 'Optimise Cluster',
                'ScriptBootstrapAction': {
                    'Path': OPTIMISE_CLUSTER_SCRIPT_LOC,
                }
            },
        ],
        Applications=[
            {
                'Name': 'Hadoop',
            },
            {
                'Name': 'Spark',
            },
        ],
        Configurations=[],
        VisibleToAllUsers=True,
        AutoScalingRole='EMR_AutoScaling_DefaultRole',
        ScaleDownBehavior='TERMINATE_AT_TASK_COMPLETION',
        EbsRootVolumeSize=10,
        JobFlowRole=JOB_FLOW_ROLE,
        ServiceRole=SERVICE_ROLE,
        ReleaseLabel='emr-5.16.0',
    )
    
    return response['JobFlowId']

# def create_step_config(warc_txt_uri):
#     """
#     Creates the python dictionary configuration needed to start a step.
#     """
#     steps=[
#             {
#                 'Name': 'Scrape CC and Parse',
#                 'ActionOnFailure': 'CONTINUE',
#                 'HadoopJarStep': {
#                     'Properties': [
#                         # {
#                         #     '': '',
#                         # },
#                     ],
#                     'Jar': 'command-runner.jar',
#                     'MainClass': 'string',
#                     'Args': [
#                         'spark-submit',
#                         '--deploy-mode',
#                         'cluster',
#                         '--master',
#                         'yarn',
#                         NEWS_PLEASE_PARSE_SCRIPT,
#                         '--num_output_partitions',
#                         '100',
#                         warc_txt_uri,
#                         'output_table',
#                     ]
#                 }
#             },
#             {           
#                 'Name': 'Push to ES',
#                 'ActionOnFailure': 'CONTINUE',
#                 'HadoopJarStep': {
#                     'Properties': [
#                         # {
#                         #     '': '',
#                         # },
#                     ],
#                     'Jar': 'command-runner.jar',
#                     'MainClass': 'string',
#                     'Args': [
#                         'spark-submit',
#                         '--deploy-mode',
#                         'cluster',
#                         '--master',
#                         'yarn',
#                         '--jars',
#                         ELASTIC_SEARCH_HADOOP_JAR,
#                         ELASTIC_SEARCH_SCRIPT,
#                         '--data_location',
#                         '/user/spark/warehouse/output_table',
#                         '--index_type',
#                         ELASTIC_SEARCH_INDEX_TYPE,
#                     ]
#                 }
#             },
#         ]

#     return steps

# def add_steps(client, cluster_id, warc_txt_uri):
#     """
#     Adds the necessary steps to a running EMR Cluster
#     """
#     response = client.add_job_flow_steps(
#         JobFlowId=cluster_id,
#         Steps=create_step_config(warc_txt_uri)
#     )
#     return response

def add_transfer_step(client, cluster_id, warc_txt_uri):
    response = client.add_job_flow_steps(
        JobFlowId=cluster_id,
        Steps=[{           
                'Name': 'Push to ES',
                'ActionOnFailure': 'CONTINUE',
                'HadoopJarStep': {
                    'Properties': [
                        # {
                        #     '': '',
                        # },
                    ],
                    'Jar': 'command-runner.jar',
                    # 'MainClass': None,
                    'Args': [
                        'spark-submit',
                        '--deploy-mode',
                        'cluster',
                        '--master',
                        'yarn',
                        '--jars',
                        ELASTIC_SEARCH_HADOOP_JAR,
                        ELASTIC_SEARCH_SCRIPT,
                        '--data_location',
                        '/user/spark/warehouse/output_table',
                        '--index_type',
                        ELASTIC_SEARCH_INDEX_TYPE,
                    ]
                }
            }]
    )
    return response

def add_parse_step(client, cluster_id, warc_txt_uri):
    response = client.add_job_flow_steps(
        JobFlowId=cluster_id,
        Steps=[{
                'Name': 'Scrape CC and Parse',
                'ActionOnFailure': 'CONTINUE',
                'HadoopJarStep': {
                    'Properties': [
                        # {
                        #     '': '',
                        # },
                    ],
                    'Jar': 'command-runner.jar',
                    # 'MainClass': None,
                    'Args': [
                        'spark-submit',
                        '--deploy-mode',
                        'cluster',
                        '--master',
                        'yarn',
                        NEWS_PLEASE_PARSE_SCRIPT,
                        '--num_output_partitions',
                        '100',
                        warc_txt_uri,
                        'output_table',
                    ]
                }
            }]
    )
    return response


def check_correct_formatted_s3_url(s3_url):
    REGEX = 's3://common-crawl-insights/spark/input/production/weekly/\d{4}/\d{2}/\d.txt'
    matches = re.search(REGEX, s3_url)
    if not matches:
        raise ValueError('Invalid S3 URL')

if __name__ == '__main__':
    # Parse arguements
    parser = argparse.ArgumentParser(description='Parse News Please Data into ES')
    parser.add_argument('s3_url', help='S3 path of WARC URL files.')
    args = parser.parse_args()

    s3_url = args.s3_url
    check_correct_formatted_s3_url(s3_url)
    client = boto3.client('emr')

    # Check for running cluster
    running_clusters = list_running_clusters(client)
    cluster_id = choose_a_running_cluster(running_clusters)
    print(f'Adding steps to existing cluster with id {cluster_id}')

    # print(cluster_id)

    # if no cluster is running, create one
    # if not cluster_id:
    #     cluster_id = create_cluster(client)
    #     print(f'Creating new EMR cluster with id {cluster_id}')

    # # add steps to cluster
    # # add_steps(client, cluster_id, s3_url)
    add_transfer_step(client, cluster_id, s3_url)
    print(f'Succuessfully added steps!')
# -*- coding: utf-8 -*-
from sparkcc import CCSparkJob
from pyspark.sql.types import StructType, StructField, StringType, LongType

import re
import sys
import json
import time

import tldextract
from bs4 import BeautifulSoup

# from newspaper import Article
# from newsplease import NewsPlease


class ExtractURLJob(CCSparkJob):
    '''Extracts pages where a list of links are present'''
    name = 'ExtractURLs'

    output_schema = StructType([
        StructField("record_id", StringType(), True),
        StructField("date_download", StringType(), True),
        StructField("url", StringType(), True),
        StructField("source_domain", StringType(), True),
        StructField("html", StringType(), True)
        
        # StructField("title", StringType(), True),
        # StructField("h1", StringType(), True),
        # StructField("description", StringType(), True),
        # StructField("text", StringType(), True)

        # StructField("language", StringType(), True),
        # # StructField("date_modify", StringType(), True),
        # # StructField("date_publish", StringType(), True),
        # StructField("authors", StringType(), True),

        # StructField("image_url", StringType(), True),
        # StructField("localpath", StringType(), True),
        # StructField("title_page", StringType(), True),
        # StructField("title_rss", StringType(), True),
    ])

    def extract_info_from_warc(self, record):
        record_id = record.rec_headers.get_header('WARC-Record-ID')

        date_download = record.rec_headers.get_header('WARC-Date')

        url = record.rec_headers.get_header('WARC-Target-URI')
        # source_domain = urllib.parse.urlparse(url).hostname.encode()
        ext = tldextract.extract(url)
        source_domain = '.'.join(ext[1:3])

        html = str(record.raw_stream.read())

        # article = Article('')
        # article.set_html(str(record.raw_stream.read()))
        # article.parse()

        # soup = BeautifulSoup(str(record.raw_stream.read()))
        # title = soup.title.text
        # h1 = soup.h1.text
        # description_tag = soup.find('meta', {'name': 'description'})
        # if description_tag:
        #     description = description_tag['content']
        # else:
        #     description = ''

        # paragraphs = soup.findAll('p')
        # text = '\n'.join(paragraph.text for paragraph in paragraphs)

        data = {
            'record_id': record_id,
            'date_download': date_download,
            'url': url,
            'source_domain': source_domain,
            'html': html
            # 'title': title,
            # 'h1': h1,
            # 'description': description,
            # 'text': text
        }

        return data

    def process_record(self, record):
        if (record.rec_type == 'metadata'
                and record.content_type == 'application/json'):
            # WAT (response) record
            return
            record = json.loads(record.content_stream().read())
            try:
                payload = record['Envelope']['Payload-Metadata']
                if 'HTTP-Response-Metadata' in payload.keys():
                    if 'HTML-Metadata' in payload[
                            'HTTP-Response-Metadata'].keys():
                        if 'Links' in payload['HTTP-Response-Metadata'][
                                'HTML-Metadata'].keys():
                            links = payload['HTTP-Response-Metadata'][
                                'HTML-Metadata']['Links']
                            for domain in domains:
                                for link in links:
                                    try:
                                        if re.search(domain, link['url'],
                                                     re.IGNORECASE):
                                            yield domain, record['Container'][
                                                'Filename'], record['Container'][
                                                    'Offset'], record['Envelope'][
                                                        'WARC-Header-Metadata'][
                                                            'WARC-Record-ID']
                                    except KeyError as e:
                                        pass
                                    except Exception as e:
                                        print(e)
                    else:
                        yield domains[0], 'no http meta'
                else:
                    yield domains[0], 'no http response'

            except KeyError as e:
                print(e)
            except Exception as e:
                print(e)

        elif record.rec_type == 'response':
            # WARC records have three different types:
            #  ["application/warc-fields", "application/http; msgtype=request", "application/http; msgtype=response"]

            try:
                #article = NewsPlease.from_warc(record)
                #yield record.rec_headers.get_header('WARC-Record-ID'), article.authors, article.description, article.filename, article.image_url, article.language, article.localpath, article.source_domain, article.text, article.title, article.title_page, article.title_rss, article.url#, article.date_download, article.date_modify, article.date_publish,

                data = self.extract_info_from_warc(record)
                yield data['record_id'], data['date_download'], data[
                    'url'], data['source_domain'], data['html']#, data['title'], data[
                        #'h1'], data['description'], data['text']
            except Exception as err:
                # print(err)
                pass

            # for domain in DOMAINS:
            #     if domain in payload.decode('latin1'):
            #         yield domain, record.rec_headers.get_header('WARC-Warcinfo-ID'), record.rec_headers.get_header('WARC-Record-ID')

    def run_job(self, sc, sqlc):
        # Redefine to remove reduction step
        input_data = sc.textFile(
            self.args.input, minPartitions=self.args.num_input_partitions)

        output = input_data.mapPartitionsWithIndex(self.process_warcs)

        sqlc.createDataFrame(output, schema=self.output_schema) \
            .coalesce(self.args.num_output_partitions) \
            .write \
            .format("parquet") \
            .saveAsTable(self.args.output)

        self.get_logger(sc).info('records processed = {}'.format(
            self.records_processed.value))


if __name__ == "__main__":
    start = time.time()
    job = ExtractURLJob()
    job.run()
    end = time.time()
    print(end - start)

# -*- coding: utf-8 -*-
from sparkcc import CCSparkJob

from pyspark.sql.types import StructType, StructField, StringType, LongType
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext

import re
import sys
import json
import time

import tldextract
from bs4 import BeautifulSoup

class ExtractURLJob(CCSparkJob):
    '''Extracts pages where a list of links are present'''
    name = 'ExtractURLs'

    output_schema = StructType([
        StructField("record_id", StringType(), True),
        StructField("date_download", StringType(), True),
        StructField("url", StringType(), True),
        StructField("source_domain", StringType(), True),
        # StructField("html", StringType(), True)
        StructField("title", StringType(), True),
        StructField("h1", StringType(), True),
        StructField("description", StringType(), True),
        StructField("text", StringType(), True)

        # StructField("language", StringType(), True),
        # # StructField("date_modify", StringType(), True),
        # # StructField("date_publish", StringType(), True),
        # StructField("authors", StringType(), True),

        # StructField("image_url", StringType(), True),
        # StructField("localpath", StringType(), True),
        # StructField("title_page", StringType(), True),
        # StructField("title_rss", StringType(), True),
    ])
    def extract_info_from_warc(self, record):
        record_id = record.rec_headers.get_header('WARC-Record-ID')

        date_download = record.rec_headers.get_header('WARC-Date')

        url = record.rec_headers.get_header('WARC-Target-URI')
        
        html = str(record.raw_stream.read())

        ext = tldextract.extract(url)
        source_domain = '.'.join(ext[1:3])

        soup = BeautifulSoup(html)
        try:
            title = soup.title.text
        except:
            title = ''
        try:
            h1 = soup.h1.text
        except:
            h1 = ''

        try:
            description_tag = soup.find('meta', {'name': 'description'})
            description = description_tag['content']
        except:
            description = ''

        try:
            paragraphs = soup.findAll('p')
            text = '\n'.join(paragraph.text for paragraph in paragraphs)
        except:
            text = ''

        data = {
            'record_id': record_id,
            'date_download': date_download,
            'url': url,
            'source_domain': source_domain,
            # 'html': html
            'title': title,
            'h1': h1,
            'description': description,
            'text': text
        }

        return data


    def process_record(self, record):
        if record.rec_type == 'response':
            # WARC records have three different types:
            #  ["application/warc-fields", "application/http; msgtype=request", "application/http; msgtype=response"]
            self.records_processed.add(1)

            data = self.extract_info_from_warc(record)
            yield data['record_id'], data['date_download'], data['url'], data['source_domain'], data['title'], data['h1'], data['description'], data['text']
        else:
            return

    def run_job(self, sc, sqlc):
        # Redefine to remove reduction step
        input_data = sc.textFile(
            self.args.input, minPartitions=self.args.num_input_partitions)

        output = input_data.mapPartitionsWithIndex(self.process_warcs)

        sqlc.createDataFrame(output, schema=self.output_schema) \
            .coalesce(self.args.num_output_partitions) \
            .write \
            .format("parquet") \
            .saveAsTable(self.args.output)

        self.get_logger(sc).info('records processed = {}'.format(
            self.records_processed.value))

    def run(self):
        self.args = self.parse_arguments()

        conf = SparkConf().setAll((
            ("spark.task.maxFailures", "10"),
            ("spark.locality.wait", "20s"),
            ("spark.serializer", "org.apache.spark.serializer.KryoSerializer"),
            ("spark.dynamicAllocation.enabled", True),
            ("spark.shuffle.service.enabled", True),
        ))
        sc = SparkContext(
            appName=self.name,
            conf=conf)
        sqlc = SQLContext(sparkContext=sc)

        self.records_processed = sc.accumulator(0)
        self.warc_input_processed = sc.accumulator(0)
        self.warc_input_failed = sc.accumulator(0)

        self.run_job(sc, sqlc)

        sc.stop()


if __name__ == "__main__":
    start = time.time()
    job = ExtractURLJob()
    job.run()
    end = time.time()
    print(end - start)

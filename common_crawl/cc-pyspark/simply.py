import requests
from warcio.archiveiterator import ArchiveIterator
import time


def print_records(url):
    boot_time = time.time()
    data = []
    records_processed = 0
    good_records_processed = 0

    with open('/Users/jai/Sites/common_crawl/cc-mrjob/cc-mrjob/crawl-data/CC-NEWS/2018/07/CC-NEWS-20180705103441-00807.warc.gz', 'rb') as stream:
        print('Opened file in {} seconds'.format(time.time() - boot_time))

        start_loop_time = time.time()
        for i, record in enumerate(ArchiveIterator(stream, arc2warc=True)):
            # print('For loop initiatited in {} seconds'.format(time.time() - start_loop_time))
            if i < 5000:
                records_processed += 1
                if record.rec_type == 'warcinfo':
                    # print(record.raw_stream.read())
                    pass

                elif record.rec_type == 'response':
                    good_records_processed += 1
                    if record.http_headers.get_header('Content-Type') == 'text/html':
                        # print(record.rec_headers.get_header('WARC-Target-URI'))
                        # break
                        data.append(record.raw_stream.read())


        print('Good records processed: {}'.format(good_records_processed))
        print('Total Records processed: {}'.format(records_processed))
        print('Finished loop in {} seconds.'.format(time.time()-start_loop_time))
    # resp = requests.get(url, stream=True)

    # for record in ArchiveIterator(resp.raw, arc2warc=True):
    #     if record.rec_type == 'warcinfo':
    #         print(record.raw_stream.read())

    #     elif record.rec_type == 'response':
    #         print(record.http_headers)
    #         if record.http_headers.get_header('Content-Type') == 'text/html':
    #             print(record.rec_headers.get_header('WARC-Target-URI'))
    #             print(record.content_stream().read())
    #             print('')
    #             break



print_records('https://archive.org/download/ExampleArcAndWarcFiles/IAH-20080430204825-00000-blackbook.warc.gz')
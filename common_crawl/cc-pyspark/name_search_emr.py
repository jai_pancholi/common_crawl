import argparse
import logging
import os
import re

from tempfile import TemporaryFile

import boto3
import botocore

from warcio.archiveiterator import ArchiveIterator
from warcio.recordloader import ArchiveLoadFailed

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.types import StructType, StructField, StringType, LongType


LOGGING_FORMAT = '%(asctime)s %(levelname)s %(name)s: %(message)s'


class CCSparkJob:

    name = 'CCSparkJob'

    output_schema = StructType([
        StructField("key", StringType(), True),
        StructField("val", LongType(), True)
    ])

    warc_parse_http_header = True

    args = None
    records_processed = None
    warc_input_processed = None
    warc_input_failed = None
    log_level = 'INFO'
    logging.basicConfig(level=log_level, format=LOGGING_FORMAT)

    num_input_partitions = 400
    num_output_partitions = 10

    def parse_arguments(self):
        """ Returns the parsed arguments from the command line """

        description = self.name
        if self.__doc__ is not None:
            description += " - "
            description += self.__doc__
        arg_parser = argparse.ArgumentParser(description=description)

        arg_parser.add_argument("input",
                                help="Path to file listing input paths")
        arg_parser.add_argument("output",
                                help="Name of output table"
                                " (saved in spark.sql.warehouse.dir)")

        arg_parser.add_argument("--num_input_partitions", type=int,
                                default=self.num_input_partitions,
                                help="Number of input splits/partitions")
        arg_parser.add_argument("--num_output_partitions", type=int,
                                default=self.num_output_partitions,
                                help="Number of output partitions")
        arg_parser.add_argument("--local_temp_dir", default=None,
                                help="Local temporary directory, used to"
                                "buffer content from S3")

        arg_parser.add_argument("--log_level", default=self.log_level,
                                help="Logging level")

        self.add_arguments(arg_parser)
        args = arg_parser.parse_args()
        self.validate_arguments(args)
        self.init_logging(args.log_level)

        return args

    def add_arguments(self, parser):
        pass

    def validate_arguments(self, args):
        return True

    def init_logging(self, level=None):
        if level is None:
            level = self.log_level
        else:
            self.log_level = level
        logging.basicConfig(level=level, format=LOGGING_FORMAT)

    def get_logger(self, spark_context=None):
        """Get logger from SparkContext or (if None) from logging module"""
        if spark_context is None:
            return logging.getLogger(self.name)
        return spark_context._jvm.org.apache.log4j.LogManager \
            .getLogger(self.name)

    def run(self):
        self.args = self.parse_arguments()

        conf = SparkConf().setAll((
            ("spark.task.maxFailures", "10"),
            ("spark.locality.wait", "20s"),
            ("spark.serializer", "org.apache.spark.serializer.KryoSerializer"),
        ))
        sc = SparkContext(
            appName=self.name,
            conf=conf)
        sqlc = SQLContext(sparkContext=sc)

        self.records_processed = sc.accumulator(0)
        self.warc_input_processed = sc.accumulator(0)
        self.warc_input_failed = sc.accumulator(0)

        self.run_job(sc, sqlc)

        sc.stop()

    def log_aggregator(self, sc, agg, descr):
        self.get_logger(sc).info(descr.format(agg.value))

    def log_aggregators(self, sc):
        self.log_aggregator(sc, self.warc_input_processed,
                            'WARC input files processed = {}')
        self.log_aggregator(sc, self.warc_input_failed,
                            'records processed = {}')
        self.log_aggregator(sc, self.records_processed,
                            'records processed = {}')

    @staticmethod
    def reduce_by_key_func(a, b):
        return a + b

    def run_job(self, sc, sqlc):
        input_data = sc.textFile(self.args.input,
                                 minPartitions=self.args.num_input_partitions)

        output = input_data.mapPartitionsWithIndex(self.process_warcs) \
            .reduceByKey(self.reduce_by_key_func)

        sqlc.createDataFrame(output, schema=self.output_schema) \
            .coalesce(self.args.num_output_partitions) \
            .write \
            .format("parquet") \
            .saveAsTable(self.args.output)

        self.get_logger(sc).info('records processed = {}'.format(
            self.records_processed.value))

    def process_warcs(self, id_, iterator):
        s3pattern = re.compile('^s3://([^/]+)/(.+)')
        base_dir = os.path.abspath(os.path.dirname(__file__))

        # S3 client (not thread-safe, initialize outside parallelized loop)
        no_sign_request = botocore.client.Config(
            signature_version=botocore.UNSIGNED)
        s3client = boto3.client('s3', config=no_sign_request)

        for uri in iterator:
            self.warc_input_processed.add(1)
            if uri.startswith('s3://'):
                self.get_logger().info('Reading from S3 {}'.format(uri))
                s3match = s3pattern.match(uri)
                if s3match is None:
                    self.get_logger().error("Invalid S3 URI: " + uri)
                    continue
                bucketname = s3match.group(1)
                path = s3match.group(2)
                warctemp = TemporaryFile(mode='w+b',
                                         dir=self.args.local_temp_dir)
                try:
                    s3client.download_fileobj(bucketname, path, warctemp)
                except botocore.client.ClientError as exception:
                    self.get_logger().error(
                        'Failed to download {}: {}'.format(uri, exception))
                    self.warc_input_failed.add(1)
                    warctemp.close()
                    continue
                warctemp.seek(0)
                stream = warctemp
            elif uri.startswith('hdfs://'):
                self.get_logger().error("HDFS input not implemented: " + uri)
                continue
            else:
                self.get_logger().info('Reading local stream {}'.format(uri))
                if uri.startswith('file:'):
                    uri = uri[5:]
                uri = os.path.join(base_dir, uri)
                try:
                    stream = open(uri, 'rb')
                except IOError as exception:
                    self.get_logger().error(
                        'Failed to open {}: {}'.format(uri, exception))
                    self.warc_input_failed.add(1)
                    continue

            no_parse = (not self.warc_parse_http_header)
            try:
                for record in ArchiveIterator(stream,
                                              no_record_parse=no_parse):
                    for res in self.process_record(record):
                        yield res
                    self.records_processed.add(1)
            except ArchiveLoadFailed as exception:
                self.warc_input_failed.add(1)
                self.get_logger().error(
                    'Invalid WARC: {} - {}'.format(uri, exception))
            finally:
                stream.close()

    def process_record(self, record):
        raise NotImplementedError('Processing record needs to be customized')

    @staticmethod
    def is_wet_text_record(record):
        """Return true if WARC record is a WET text/plain record"""
        return (record.rec_type == 'conversion' and
                record.content_type == 'text/plain')

    @staticmethod
    def is_wat_json_record(record):
        """Return true if WARC record is a WAT record"""
        return (record.rec_type == 'metadata' and
                record.content_type == 'application/json')
# -*- coding: latin-1 -*-


from pyspark.sql.types import StructType, StructField, StringType, LongType
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext

import re
import sys
import json
import time

import tldextract
from bs4 import BeautifulSoup

NAMES = ['The All England Lawn Tennis & Croquet Club Ltd','WEST COAST SERVICE PROVISION LTD','TRANSPORT FOR LONDON','STEWARDS COMPANY LTD','WILSON JAMES LTD','NEWSHIP LTD','JENKS & CATTELL LTD','BRITISH PIPELINE AGENCY LTD','MAPELEY COLUMBUS HLDGS LTD - UK','The P & M Group Ltd','Mapeley Steps Contractor Ltd','SEELE GMBH - UK','GEMECH FOUNDATIONS GROUP LTD','RACE MARKETING ASSOCIATES LTD','LEE BARON GROUP LTD','H.E. Simm & Son Ltd','Connect Plus \(M25\) Ltd','CANAVAX LTD','NEW TRIBECA LTD - UK','C.D.S. \(Oil & Gas Services\) Ltd','THALES HOLDINGS UK PLC','SQUARE ENIX LTD','ROAD MANAGEMENT SERVICES \(A13\) PLC','Brookfield Construction Ltd','UNDERHILL ENGINEERING LTD','EDPR UK LTD','JOHN F HUNT GRP LTD - UK','S&K CAR PARK MANAGEMENT LTD','LAING OROURKE CONSTRUCTION LTD','CALEDONIAN HERITABLE LTD','LINDUM GROUP LTD','ENSIGN HIGHWAYS LTD','SALREX LTD','THE 100 BISHOPSGATE PARTNERSHIP','Tata Steel Europe Ltd','HENDERSON RISK LTD','ABU DHABI AIRPORTS CO PJSC','AMERICAN BRIDGE HOLDING CO','JD POPE & SONS LTD','BOWMER & KIRKLAND LTD','FENROSE LTD','TEIGHMORE LTD','ERH HOLDINGS LTD','WELSH POWER GROUP LTD','HITACHI ZOSEN INOVA UK LTD','VICTOR BUYCK STEEL CONSTRUCTION LTD - BE','IGNIS WICK LTD','H2O PLUMBING & HEATING SERVICES LTD','JONS CIVIL ENGINEERNG COMPANY LTD','L A Moore Ltd','SIRIUS MINERALS PLC','THE OXFORD ASPHALT CO. LTD','MIDLAND FELT ROOFING LTD','PROCOMM SITE SERVICES HOLDINGS LTD','LAND SECURITIES GROUP PLC','TATA INTERNATIONAL METALS UK LTD','BALCAN LTD','T BOURKE & CO LTD & HEALTH SVCS EXEC- IE','BRITISH STEEL LTD','LYDIAN INTERNATIONAL LTD','CARILLION PLC','COPUNO UK LTD','UBB WASTE \(ESSEX\) LTD','INNOVATIVE ENVIRONMENTAL SOLUTIONS UK LTD','NORTHERN GAS NETWORKS OPERATIONS LTD','TECHOEDIL CONSTRUCTORA SA','MAOR SCAFFOLDING LTD','ENERGETICS DESIGN & BUILD LTD','PX UK HOLDCO1 LTD','CLARK CONTRACTS LTD','NATIONAL GRID PLC','WORLD FUEL SERVICES EUROPE LTD','SWEEPTECH ENVIRONMENTAL SERVICES LTD','CARILLION MORGAN SINDALL JV - UK','MERSEY GATEWAY BRIDGE PROJECT - UK','ERNEST WEST & BEYNON LTD','CET GRP HLDGS LTD & SUBSIDIARY COMPANIES','IB VOGT GMBH - UK','CASTALUM LTD','CAPITAL & COUNTIES PROPERTIES PLC','LIGHTWAYS CONTRACTORS LTD','GLASGOW CITY COUNCIL','JOHN SWIRE & SONS LTD','BLUEVALE STRUCTURES LTD - UK','LAING OROURKE PLC','F3 GRP LTD - UK','AMEYSERSA JOINT VENTURE','UNITED AUTOMOTIVE INTERIORS LTD','CSEC4 LTD','CHEMCEM SCOTLAND LTD - GB','ECOLUTIA SVCS AG - UK','WESTGATE OXFORD ALLIANCE LP','COOKE AQUACULTURE SCOTLAND LTD - UK','KRONOS SOLAR GMBH - UK','S WALSH HOLDINGS LTD','ONE GROUP CONSTRUCTION LTD','PARABEL UK LTD','ROYALTON GROUP HOLDINGS LTD','MARINE SPECIALISTS LTD','NPH HEALTHCARE LTD','POWERTEAM ELECTRICAL SVCS LTD - GB','D W R Cymru Welsh Water','SYNTEX ENGINEERING SERVICE LTD - UK','WHEATLEY HLDGS LTD - UK','INSPECTAHIRE INSTRUMENT CO LTD - UK','ALUN GRIFFITHS \(CTRS\) LTD - UK','WATES GROUP LTD','JIFFY UK HOLDING I LTD','COSTAIN LTD A465 DUALLING - UK','A & T SVCS LTD - UK','CREST NICHOLSON \(EPSOM\) LTD - UK','VARIAN MEDICAL SYSTEMS UK LTD - UK','TRINITY LONDON ACADEMY TRUST','BAM PPP UK LTD','GRAF UK LTD - UK','BROOKFIELD UTILITIES UK HOLDINGS LTD','BUUK INFRASTRUCTURE HLDGS LTD - UK','HEADCROWN GROUP PLC','COPTHORN HOLDINGS LTD','PERRYS GROUP LTD','DV4 PROPERTIES RMC CO LTD - UK','HESIS LTD','PTS \(SA\) LTD - UK','NEWTON HOLDINGS LTD','REDFLEX TRAFFIC SYSTEMS LTD','NSMP TGPP LTD','WOLF MINERALS UK LTD','John Wood Group P.L.C.','PURCELL CONSTRUCTION LTD','MUNICIPAL WASTE RECYCLING LTD - UK','HUCK NETS U K LTD','QUINNS CONSTRUCTION LTD','TTAG LTD','MEDIPARK LTD - UK','PS1117 HOLDINGS LTD','BK LTD','NUCLEAR DECOMMISSIONING AUTHORITY','TEIGNMOUTH MARITIME SERVICES LTD','NATIONAL OIL RESERVES AGENCY LTD - IE','HUB WEST SCOTLAND PORTFOLIO - UK','BBK CONSTRUCTION SERVICES LTD','GRANGEWOOD BUILDERS LTD - UK','NATIONAL GAME FAIR LTD','TEAM WIGGINS LTD','MADISON DEVELOPMENTS LTD','METRO DECONSTRUCTION SERVICES LTD','City Of Birmingham Council','ECORP OIL & GAS UK LTD','MACE FINANCE LTD','AWE PLC - UK','J & M MURDOCH & SON LTD - UK','JSL GRP LTD - IE','WORLD FUELS SVCS EUROPE LTD - UK','INTL ENERGY GRP LTD - GG','ALBANY MOLECULAR RESEARCH LTD','LONSDALE HOLDINGS LTD','INCEPTION HLDGS SARL - UK','JADE ADEN SERVICES LTD','JJC HIRE LTD','D & R GROUP PLC','GLENALMOND TIMBER CO LTD','GALLIFORD TRY PLC','DEEPOCEAN GRP HLDG BV','DCUK FM - UK','HENRY W POLLARD & SONS GROUP LTD','MCKENZIE GRP - UK','MCKINSTRY SKIP HIRE LTD','J GUNN SCAFFOLDING LTD','JAMES MERCER GROUP LTD','GAP HLDGS & SUBSIDIARY CO','SANDSFIELD GRAVEL CO LTD - UK','COUNTRYSIDE MARITIME LTD','ASCON INDUSTRIAL ROOFING LTD - UK','WELLMAX SCAFFOLDING LTD - UK','COLLIER ENVIRONMENTAL SERVICES LTD','K & I SITE SERVICES LTD','BARTOLINE LTD','GREATER GRP GLOBAL PTY LTD - UK','MACE LTD','INTERNATIONAL ENERGY GROUP LTD - GG','TECNICAS REUNIDAS UK LTD - UK','OLIVER CONNELL & SON LTD','DESSA LTD - UK','BELLE HOLDINGS LTD','ALTRAD NSG LTD','ALTRAD BEAVER 84 LTD','TRAD SAFETY SYSTEMS LTD - UK','ALTRAD MTD LTD','TRAD HIRE & SALES LTD - UK','TRAD GROUP LTD','GENERATION HOLDINGS LTD','SCI ENVIRONMENTAL GRP LTD - UK','THE ROYAL EDINBURGH MILITARY TATTOO LTD','CRUDEN GRP - UK','CHEVRON TRAFFIC MANAGEMENT HOLDINGS LTD','FORDS SOUTH WEST LTD','MACE UCLH EAR NOSE & THROAT CLINIC - UK','MUSE DEVELOPMENTS LTD','ANADOLU AJANSI TURK AS.TRT WORLD - TR','32 ASTONS ROAD - UK','AURELIUS ENVIRONMENTAL LTD','MEL GRIFFITHS LTD','HALLMARK REAL ESTATE GROUP LTD','HYUNDAI MOTOR UK LTD','ELECLINK LTD - FR','HARBORO RUBBER CO LTD THE','TOWER HAMLETS SCHOOLS LTD','ACTIVE TUNNELLING LTD - UK','QUINTO CRANE & PLANT - UK','COFFEY CONSTRUCTION LTD - IE','GREENHOUS GRP HLDGS LTD - UK','FRAMACO INTL, INC - PG','COASTVIEW ESTATES LTD - UK','TECHPLAY S.A SOFTWARE LTD - IL','U. FEMINI DEVELOPMENT & INVESTMENTS LTD','Balfour Beatty Capital Ltd','UKD GROUNDWORK & CIVIL ENG LTD - UK','AFRIDAR E.L.I LTD - IL','COMPLETE MOLING SVCS \(SOUTH EAST\) - UK','ALLOY MTD \(JERSEY\) LTD - UK','D KETLEY \(ASPHALT\) LTD - UK','J J DONOVAN & SON LTD - UK','CHINA NAVIGATION CO LTD - UK','LEATHAMS HLDG LTD &/OR BARNES- UK','ZENIUM TECHNOLOGY PTNRS - UK','FAIRFORD MEDICAL LTD - UK','HILAL BIL BADI & PTNRS CONTRACTING CO-AE','M/S MAJID FOUTAIM SARL - LB','ENCO HLDGS LTD - UK','ROM KINNERET INVESTMENTS LTD - IL','MONOROOF LTD &/OR PAVPAD LTD - UK','COMER GRP - UK','NORTHERN & SHELL INVESTMENTS NO 2 LTD-UK','DXI REGENERATION LTD - UK','WATTS OF LYDNEY GRP LTD - UK','HISTORIC ROYAL PALACES - UK','RUNFOLD DEMOLITION & SALVAGE LTD - UK','GREAT ORMOND STREET HOSPITAL CHILDRENS','NAVEH GAD BUILDING DEVELOPMENT LTD','TRENCH CONTROL \(NI\) LTD - UK','SREGAL SINCLAIR LLP - UK','LOCAL GOVERNMENT ASSOCN LGA - UK','ASHWORTH DEMOLITION - UK','GODFREY CONSTRUCTION LONDON LTD - UK','BAUER RENEWABLES LTD - UK','TUDORBORNE LTD T/A NEARY CONSTRUCTION','DUGGAN ALLIED STEEL LTD - IE','PICTURE HOLDCO LTD','SHEFFIELD CITY COUNCIL - UK','HAIG LTD - EQUINOX ST JAMES LTD - UK','SPIDER PROJECT MGT LTD - UK','RE SOL LTD - UK','ONLINE TOOL HIRE &/OR ONLINE DRILLING-UK','MATERIALS MOVEMENT LTD - UK','KXEN LTD - UK','MCKENZIES \(NI\).LTD','GOVERNMENT OF LEBANON PRESIDENTS OFFICE','SELA CONSTRUCTION & INVESTMENTS LTD- IL','CHESTERFIELD HOUSE PTNRS LLP - UK','TOWNGATE ETATES LTD - UK','AL BANDARY ENG TRADING & CONTRACTING','JOHN SISK & SON LTD','THAMES PTNRSHP FOR LEARNING LTD - UK','Group 1St Ltd','TENNET OFFSHORE DOLWIN3 GMBH & CO - NL','TRANSEUROPEAN V RESIDENTIAL \(JERSEY\) LTD','MID WALES HOUSING ASSOCN - UK','RAYLUTE LTD - UK','WOODCOTE ESTATES LTD - UK','CAT INTL CO LTD - SA','IMPERIAL OCEAN PLAZA LTD - UK','NATHAN RASKIN LAWYER CO - IL','Costain Group Plc','COSTAIN GRP PLC - EMIRATES INS CO - AE','COSTAIN LTD & CH2M HILL UK LTD - UK','STONEGROVE REFRIGERATION SVCS LTD - UK','EPPING INVESTMENT HLDGS LTD - UK','J C SVCS \(ANGLIA\) LTD - UK','B&R GATESHEAD LTD - UK','WEST HOUGHTON ROOFING & POINTING SVCS','ROBERTSON GRP LTD - UK','TOLLEY MAHON LTD - UK','HER MAJESTY THE QUEEN OF ALBERTA - UK','HIGSONS 1780 LTD -UK','OFER BROTHERS - ENG & DEVELOPMENT LTD.','IQA OPERATIONS GROUP LTD','ALL SEA OFFSHORE ASIA LTD - CN','ROBERTSON HEALTH \(ORKNEY\) LTD - UK','GF TOMLINSON GRP LTD - UK','M&J DRILLING HLDG LTD - UK','EDWARD DEWHURST LTD - UK','PEEL MEDIA DEVELOPMENT RESIDENTIAL 1 LTD','CORRAMORE CONSTRUCTION LTD - UK','CONNOLLY & CALLAGHAN LTD - UK','Kdc Contractors Ltd','CHASE \(CASSIO\) LTD - UK','PRISTINE LONDON - UK','FUSION CARDIFF QUARTER LLP - UK','RUSKIN PROPERTIES LTD - UK','VIRIDOR WASTE MANAGEMENT LTD','AZORIM CONSTRUCTION \(1965\) LTD - IL','COSTAIN LIMITED & GALLIFORD TRY INFRAS','Transport For Greater Manchester','Rolls-Royce Holdings Plc','EPS EVENTS LTD - UK','AURA ISRAEL DEVELOPMENT & INVESTMENTS','SHROPSHIRE HOMES HLDGS LTD & SUB - UK','ALLIED LONDON PAVILION DEVELOPMENTS LTD','KINGS COLLEGE LONDON','CARRAIG INVESTMENTS SARL - UK','NOTTING HILL HOME OWNERSHIP LTD - UK','CENTER PARCS IRELAND LTD - IE','KDM HURST STREET LTD - UK','TRACKWORK GRP LTD & SUBSIDIARY CO - UK','CPL INDUSTRIES GROUP LTD','S&B UTILITIES LTD - UK','CHINA GEZHOUBA GRP CO LTD - KW','PHOENIX INS CO LTD - RAMOT MALL EXPAN-IL','JEAKINS WEIR LTD - UK','TI INDUSTRIAL GRP LTD - UK','NATIONAL GRID ELECTRICITY TRANSMISSION PLC','VALOR REAL ESTATE PTNRS - UK','HOLMES LYNDON TRUST LTD - UK','BOUSKILA BROTHERS LTD - IL','CLERE AG SCHLUTERSTR - UK','EVERTON FOOTBALL CLUB CO LTD','EXETER CITY AFC LTD - UK','PULLMANS TAVERN GRAYS - UK','AHMAD YOUSEF AL TARAWNEH - JO','CAPITAL & COUNTIES CG LTD - UK','H CO 2 LTD - UK','AL RASHID CONTG & ICDAS CONST - SA','KESHAVA LONDON RESIDENTIAL LLP - UK','TRICOYA VENTURES UK LTD - UK','AL ARAB CONTRACTING CO - SA','MARYLEBONE INTERIORS LONDON LTD - UK','FORMULA ONE MGT LTD - UK','GRAND SOUTH LTD - UK','BCM SCAFFOLDING SVCS LTD - UK','UNIVERSITY OF BEDFORDSHIRE - UK','COMBINED GRP CONTRACTING - KW','ASPIN GRP HLDGS LTD - UK','BERESHIT TOWERS DANIEL REAL ESTATE LTD','DETRAFFORD ESTATES ELISABETH GARDENS LTD','EURO-CITY GROUP LTD - UK','PETROFAC GRP OF COMPANIES - AE','PORTSMOUTH COMMUNITY FOOTBALL CLUB LTD','MACDONALD GROUNDWORKS LTD - UK','DR SHECHTER MORDECHAI','MINISTRY OF PUBLIC WORKS','STABLE EXECUTIVE INVESTMENTS LTD - UK','RABINOVICH YEHOSHUA CONSTRUCTION & INVES','EXXONMOBIL PNG LTD - AU','WEST MIDLANDS COMBINED AUTHORITY - UK','DIRECTORATE GENERAL OF CIVIL AVIATION-KW','MCLAREN TECHNOLOGY GROUP LTD','MCLAREN AUTOMOTIVE LTD - UK','THE ARSENAL FOOTBALL CLUB PLC','DENHOLM FISHSELLING LTD','ALONEI OFEK LTD - IL','PLEXAL \(CITY\) LTD - UK','GOOGLE UK LTD','EQUITABLE HOUSE LTD -UK','M/S WASL MGT CORP. - AE','MELDRUM CONSTRUCTION LTD - UK','ELECTRICITE DU LIBAN \(EDL\) - LB','S WILKIN & SONS LTD - IE','AXIMOR LTD - UK','DAN PUBLIC TRANSPORT CO. LTD - IL','BUCHANAN SKIP HIRE LTD - UK','BCEGI CONSTRUCTION \(UK\) LTD - UK','ENERGETICKY A PRUMYSLOV HLDG - CZ','AZRIELI GROUP LTD','STADLER RAIL SERVICE UK LTD - UK','BACHY SOLETANCHE HLDGS EUROPE LTD','HUB SW CUMBERNAULD DBFM CO LTD - UK','K&T HLDGS LTD - UK','AMERY CONSTRUCTION LTD - UK','URBAN&CIVIC PLC','MCLAREN PROPERTY LTD - UK','BOULTBEE BROOKS \(ORSMAN\) LTD - UK','AMAZON UK SVCS LTD - UK','MIVNIM & NETIVIM LTD - IL','GINDI BASIC LTD PTNRS - IL','TONY KIRWAN CONTRACTORS LTD - UK','WESTMINSTER FIRE STATION LTD - UK','MOUNTVIEW ACADEMY OF THEATRE ARTS - UK','Saudi Arabian Glass Company','HAMPTON SCHOOL - UK','K J PICKERING LTD - UK','GRANGEWOOD BRICKWORK SVCS LTD - UK','VENABLES PROPERTIES LTD - UK','DUSHIK LTD - IL','ALLIED EXPLORATION - UK','HARTPURY COLLEGE - UK','XYZ WORK LTD - UK','TAAVURA GRP - IL','WILLIAM PEARS GRP - UK','CAESAREA DEVELOPMENT CORP ADMOND BYNIA','WESCOTT COATINGS & TRAINING SERVICES LTD','GINDI ISRAEL 2010 LTD - IL','SKYMIST HLDGS LTD - UK','GALBRAITH HOLDINGS LTD','JTLV INVESTMENTS LTD - IL','METSADA 3 LTD - IL','MULALLEY & CO LTD - UK','AQABA DEVELOPMENT CORP - JO','1-9 KINGS RD LLP - UK','DAVID SEA & PARK ACQUISITION GRP - IL','ALTERNATIVE INVESTMENT MANAGEMENT LIMITED','ASHTROM CONSTRUCTION CO NORTH LTD - IL','AMOT INVESTMENTS LTD','Strathclyde Partnership For Transport','DOR CHEMICALS LTD','REGAL HACKNEY ROAD LTD - UK','UNICITY XXVII BOURNEMOUTH 3 SARL - UK','ECONOMIC CO FOR DEVELOPMENT OF MODIIN CI','BSR ENG AND DEVELOPMENT LTD - IL','The City Of Edinburgh Council','TEAM INDUSTRIAL \(UK\) LTD T/A TEAM SO','UK REAL ESTATE LTD - UK','CLALIT HEALTH SERVICES DOCTORS ORGANIZATION','COPECROWN CONSTRUCTION LTD - UK','REYNOLDS & READ LTD - UK','DOW WASTE MGT LTD - UK','SMS SECURITY CONSULTANC LTD - MT','BILTON & JOHNSON \(BLDG CO\) LTD - UK','LEGAL & GENERAL GROUP PLC','MIGDALEI ISRAEL','GCP BLOOMSBURY LTD - UK','ACHUZAT NOFIM PURCHASING GRP - IL','BEE JAY SCAFFOLDING LTD - UK','YUVAL ALON CONSTRUCTION CO LTD','HELICAL \(DURRANTS\) LTD - UK','HARRISONS NORTH WEST LTD','MACE DEVELOPMENTS \(EXETER\) LTD - UK','T M WARD \(DARLINGTON\) LTD','PEEL MEDIA CANALSIDE LTD - UK','NRS GRP & NOEL P REGAN DEVELOPMENTS LTD','YH DIMRI AZORIM BEER SHEVA LTD PLOT 101','FWS CARTER - UK','EURO-CITY EASTERN - UK','ADVANTE LTD - UK','WATKIN JONES GROUP LTD','ERITH HOLDINGS LTD','SERFIS CONSTRUCTION & ENG LTD - UK','ALUFER MINING LTD - GG','BOOHOO.COM UK LTD - UK','MUNNELLY LOGISTICS - UK','HALTON BOROUGH COUNCIL','SKONTO ENTERPRISES UK LTD - UK','ARDROSS INVESTMENTS LTD - UK','AA RUBINSTEIN SENIOR HOUSING - IL','SHIRE PHARMACEUTICALS IRELAND LTD - IE','MK GALLERY - UK','CASTLEVIEW PLANT - UK','SIAC CONSTRUCTION LTD - IE','TIDHAR CONSTRUCTION LTD','WILLIAM KING LTD - UK','CMM CONTRACTORS LTD - UK','GULF STEVEDORING - JEDDAH PORT','A.I NEVO LTD - IL','MCCAIN FOODS \(GB\) LTD - UK','SWAN UNITED SVCS - UK','DURKAN ESTATES LTD - UK','MACE DEVELOPMENTS \(CARDIFF\) LTD - UK','PRIOR & CO \(SW\) LTD &/OR MARDEN ROOFING','TEVA PHARMACEUTICAL INDUSTRIES LTD - DE','RWC PTNRS LLP - UK','HOT ENGINEERING & CONSTRUCTION CO - KW','CADMAN CRANES LTD - UK','AMELCO UK LTD - UK','EXPERTISE CONTRACTING CO - SA','RIGHTACRES PROPERTY CO LTD - IL','HUB SW QMA DBFM CO LTD - UK','AL QABDAH BLDG CONTRACTING - AE','ACHIM RACHMA LTD - IL','INVESTEC BANK PLC - UK','EILAT ASHKELON PIPELINE CO LTD','RIDGEONS LTD - UK','LYMAR CONTRACTS LTD - IE','GINDI GARDENS LTD - IL','SKY VIEW ESTATES LTD - UK','LONDON TROCADERO 2015 LLP & CRITERION','BOW ST HOTEL LTD - UK','WEST END PROPERTY GENERAL PTNRS - UK','SKY BUILDING NO 1 LTD - UK','AREA ESTATES LTD - UK','MCLAREN \(BALDWIN STREET\) LTD - UK','DOGS TRUST PROMOTIONS LTD','RAK HOSPITAL - AE','S.R GINOT DAVID - IL','GENERAL NUCLEAR INTL LTD - UK','ESPANIOL RIAD LTD - IL','W FERGUS TA CENTRAL TREE SURGEONS - UK','LSRM HLDGS LTD - UK','JACKSON DRILLING HLDGS LTD - UK','HARRIS CALNAN HLDGS - UK','A & B CTRS \(DEVON\) LTD - UK','AL JABER HEAVY LIFT LLC - AE','TOWER DEMOLITION LTD - UK','57 WHITEHALL S.A.R.L. - UK','BELLCROSS CO LTD - UK','ROOMZZZ NEWCASTLE - UK','LMS HIGHWAYS LTD - UK','RISHON LEZION ECONOMIC CO LTD - IL','ELC \(BARROW\) LTD - UK','MR CONCRETE \(IRELAND\) - IE','Interserve Plc','ALPHASTRUT LTD - UK','COUNTRYSIDE 26 LTD','URBAN SPLASH \(BROWNSFIELD MILL\) LTD - UK','RAVEN DEVELOPMENT HOMES LTD - UK','AKM DA VINCI IN KANARIT LP - IL','ST MODWEN PROPERTIES PLC','COUNTRYSIDE ZEST \(BEAULIEU PARK\) LLP','PEAK WASTE RECYCLING LTD - UK','SPT - BRIDGE ST - UK','KOBI ZION LTD - IL','MENASHE RAHAMIM & SONS LTD - IL','CAMPBELL COURT PROPERTY INC - UK','K & S BUILDINGS LTD - UK','AUDLEY GROSVENOR LTD - UK','M/S AL BAKER INVESTMENTS - QA','SUPERSCHEME LTD \(WATKINS JONES GRP\) - UK','HAGAG GRP - IL','LAWSONS HLDGS LTD - UK','ARCO LTD - UK','TIDE CONSTRUCTION LTD','ICOPAL LTD','HEATH CRAWFORD & FOSTER LTD - UK','YORK ROOMZZZ - UK','OSHIRA LTD','ORION ADVANCED ENTREPRENEURS LTD - IL','COMBINED FACILITIES MANAGEMENT LTD - UK','WESTERN WAY RETAIL LLP - UK','HIDRONORMANDIA S.A','AFRICA ISRAEL PROPERTIES LTD & MELISRON','GREEN ENERGY GEOTHERMAL INTL LTD','AMERICAN SAVINGS BANK FSB','VS 125 LLC','RENAKER BUILD LTD','INEOS USA LLC','SAPPI NORTH AMERICA - UK','OC 405 PARTNERS JV','SAMMON GRP','NEW FARM DEVELOPMENTS LIMITED','ANEL CONTRACTING & ENG LTD','PHE CONSTRUCTION LTD- GB','SALACH DABACH & SONS','CSL BEHRING AG - CH','WHYTE GROUP LTD','THE HALO TRUST','ALDERNEY SHIPPING CO LTD','ROAD MGT SVCS A13 PLC- UK','RADEC ENG LTD','BRITISH HORSERACING AUTHORITY LTD','HARGREAVES SERVICES PLC','FORMULA ONE MANAGEMENT LTD','DESTINATION EVENTS LTD','ATLAS SERVICES GROUP BV','GLOBAL TUNNELLING EXPERTS UK LTD','FLYING PICTURES GROUP LTD','DELTA MOTORSPORT LTD','SEELE GMBH','GEG HOLDINGS LTD','IMPETUS AUTOMOTIVE LTD','GEMECH FOUNDATIONS GRP LTD','ENERMECH GROUP LTD','ROBBIE GETHIN ASSOCIATES LTD','BALLYMORE PROPERTIES LTD','IHC IQIP UK LTD','WELLWISE INTL \(L\) LTD','HELIX WELL OPS \(U.K.\) LTD','AMBER LOUNGE LTD','AMERICAN BRIDGE INTL CORP','AVOCET MINING PLC','NETWORK STRATIGRAPHIC CONSULTING LTD','PAT MUNRO \(ALNESS\) LTD','TECHNIP SINGAPORE PTE LTD','LEE BARON LTD','JD POPE & SONS LTD - UK','MEDICAL PIPED GASES LTD','JOHN F HUNT GRP LTD - GB','VIRIDIAN GROUP LTD','BROOMPARK HOLDINGS LTD','SPIE OIL & GAS SVCS ASP SDN BHD','Perenco U K Ltd','BARTECH MARINE ENGINEERING LTD','NUSTEEL HOLDINGS LTD','SAFFRON MARITIME LTD','SID DENNIS & SONS LTD','SITE SOLUTIONS \(TD\) LTD','VINCI ENERGIES UK HOLDING LTD','KEMER LTD','IM JERSEY HLDGS LTD','ENTREPRISE DELECTRICITE ET DEQUIPEMENT','DHG HYDRO LTD','KVAERNER ASA','WELSH POWER GRP LTD','VICTOR BUYCK STEEL CONSTRUCTION LTD','GLOBAL INFRASTRUCTURE SCOTLAND LTD','FORDSTAM LTD','ADDISON PLANT LTD','W MUSKER RIGGERS LTD','TATA INTL METALS \(UK\) LTD','WHYTE INTL LTD','LYDIAN INTL LTD','HYDRO INDUSTRIES LTD','GESTION MULTI SERVICES LTD','AAF MCQUAY UK LTD','FREYSSINET LTD','SWEEPTECH ENVIRONMENTAL SVCS LTD','C SEC 4 LTD','ECOLUTIA SERVICES AG','OCHL \(GLOBE\) LTD','GENRIC HLDGS LTD','BEAZLEY 623 SYNDICATE\(REASS\)','LONDON STADIUM 185','AKASTOR ASA','SEARCHWISE LTD','POWERTEAM ELECTRICAL SERVICES \(UK\) LTD','INSPECTAHIRE INSTRUMENT CO LTD','JIFFY PACKAGING CO LTD','BROOKFIELD UTILITIES UK HLDGS LTD','WOLF MINERALS \(UK\) LTD','MUNICIPLE WASTE RECYCLING LTD','WILLS BROS LTD','GRANGEWOOD BUILDERS LTD','TEAM WIGGINS','METRO DECONSTRUCTION SVCS LTD','J & M MURDOCH & SON LTD','INTL ENERGY GRP LTD','ALBANY MOLECULAR RESEARCH UK LTD','DCUK FM','JON GUNN SCAFFOLDING LTD','SANDSFIELD GRAVEL CO LTD','ASCON INDUSTRIAL ROOFING LTD','WELLMAX SCAFFOLDING LTD','COLLIER ENVIRONMENTAL SVCS LTD','TECNICAS REUNIDAS UK LTD','DESSA LTD','TRAD SAFETY SYSTEMS LTD','TRAD HIRE & SALES LTD','GENERATION HLDGS LTD','SCI ENVIRONMENTAL GROUP LTD','ANADOLU AJANSI TURK A.S./TRT WORLD','VINCI CONCESSIONS UK LTD - GB','QUINTO CRANE & PLANT LTD','CHINA NAVIGATION CO LTD','TRENCH CONTROL \(NI\) LTD','BAUER RENEWABLES LTD','MATERIALS MOVEMENT LTD','HER MAJESTY THE QUEEN OF ALBERTA','ALL SEA OFFSHORE ASIA LTD','ENERGETICKY A PRUMYSLOVY HOLDING AS','GRANGEWOOD BRICKWORK SVCS LTD','NEWHEAT MECHANICAL SVCS LTD','ALUFER MINING LTD','AMELCO UK LTD','SI & P LTD','THE RACECOURSE ASSOCIATION LTD','ARENA HOUSING GROUP LTD','ROSEBERY MEWS MENTMORE MGT CO LTD','MONTAGU EVANS','NEW TRIBECA LTD','BARBOUR ALL TERRAIN TRACKING LTD','WABTEC UK HOLDINGS LTD','BALCAN ENG LTD','METALS EXPLORATION PLC','IB VOGT GMBH','BLACKLAND PARK EXPLORATION LTD','THYOLO NUT CO LTD','COOKE AQUACULTURE SCOTLAND LTD','KRONOS SOLAR GMBH','CREST NICHOLSON \(EPSOM\) LTD','VARIAN MEDICAL SYSTEMS UK LTD','BUUK INFRASTRUCTURE HLDGS LTD','CUADRILLA RESOURCES HOLDINGS LTD','DV4 PROPERTIES RMC CO LTD','NORTH SEA MIDSTREAM PTNRS LTD','HUB WEST SCOTLAND LTD','MCKENZIE GRP PRACTICE','HARBORO RUBBER CO LTD','NGS BUILDING  SVCS LTD','TENNET OFFSHORE DOLWIN3 GMBH & CO KG','CLERE AG, SCHLUTERSTR','PLEXAL CITY LTD','ARLINGTON AUTOMOTIVE LTD','MULALLEY & CO LTD','UK REAL ESTATE LTD','SMS SECURITY CONSULTANCY LTD','ALPHASTRUT LTD - GB','EasyJet Plc','BRITVIC PLC','ADGM LTD','JAMES FISHER & SONS PLC','PRAMERICA REAL ESTATE INVESTORS LTD','Challenger DPG France SAS +','Stella Investments SP zo.o','TMW Pramerica Property Investment GmbH \(Finland\)','TMW Pramerica Property Investment GmbH\(Netherlands\)','HOMESERVE PLC','ANGLO BEEF PROCESSORS','Fendercare Australia Pty Ltd','Fendercare Marine \(Middle East\)','Fendercare Marine Asia Pacific','MACLAREN UK LTD','MACTAGGART SCOTT \(HOLDINGS\) LTD','MACTAGGART SCOTT AUSTRALIA PTY LTD','Alexander Dennis Limited+','JOHN MENZIES PLC','Stahl Holdings B.V.','Angus Fire Ltd','HY-TEN GROUP LTD','Hy-Ten LLC','NATIONAL FOAM INC','ENERMECH QATAR LLC','AIR MENZIES INTERNATIONAL \(AUST\) PTY LTD','Menzies Aviation Santo Domingo','AHS Ghana Ltd','AHS \(Jordan\) Ltd','MENZIES AVIATION MEXICO S A DE CV','Air Menzies International \(Netherlands\) BV','MENZIES AVIATION NEW ZEALAND LTD','AIR MENZIES INTERNATIONAL CPT \(PTY\) LTD','MENZIES AVIATION \(TEXAS\) INC.','HONEY NEW ZEALAND -EUROPE LIMITED','ABP Polska Pa. zoo','STAHL COATINGS & FINE CHEMICALS SUZHOU CO LTD','STAHL ASIA PACIFIC PTE LTD','STAHL KIMYA SANAYI VE TICARET LTD SIRKETI','STAHL USA INC','XTRAC TRANSMISSIONS LTD','AMCO DRILLING MALI SARL','XTRAC INC','Key Assets the Childrens Services Provider Ltd','Wellwise International Ltd','JFD South Africa \(Pty\) Ltd','ORION ENG SVCS LTD','POWER SYSTEMS CONSULTANTS NEW ZEALAND LTD','OILFIELD PRODUCTION CONSULTANTS OPC LTD','DSL Derrick Services UK Ltd','TENNANTS CONSOLIDATED LTD','United States Cold Storage, Inc','Derrick Services Middle East FZE','ATLAS SERVICES GROUP HOLDING BV','FENDERCARE NIGERIA LIMITED','ESO EUROPEAN SOUTHERN OBSERVATORY','Fort Vale Limited','Orion Personnel Phillippines Inc','SHALEEM INTERNATIONAL LLC','Orion Project Svcs Qatar','easyJet Switzerland SA','SWIRE OILFIELD SERVICES LTD','FORT VALE PTE LTD','INNOVIA FILMS \(HOLDING 1\) LTD','Atlas Services Group LLC','PRIME POWER ANGOLA','Productos Stahl de Colombia SA, Clariant\(Colombia\) SA','Zenium Holdings Ltd','Rutherford Global Power Europe Ltd','Oilfield Production \(OPC\) Asia LLP','Innovia Films \(Holding\) Pty Ltd','Innovia Films, S.A. de C.V.','Innovia Films, Inc.','KCAD HOLDINGS I LTD','KCA Deutag Ltd','Cucina Lux Investments Ltd and all SubsidiaryCompanies','Wormvax Ltd','Swire Cold Storage PTY. LTD.','KCA DEUTAG LTD','KCA Deutag Caspian Representative Office','KCA Deutag Nigeria Ltd','OMAN KCA DEUTAG DRILLING CO LLC','KCA DEUTAG DRILLING GMBH','Performance Drilling for Oil Services Ltd','KCA Deutag LLC','KTSA DOITAG DRILLING GMBKH FILIAL','European Southern Observatory','Enermech Australia','Al-Fuz Project Management Services LLC','Orion Engineering Services Liberia Ltd','Orion Group KZ Too','Orion Project Services LLC +','The Moredun Foundation +','Genea Oxford Fertility LTD','DIRECT WINES HOLDINGS LTD','FLOOD CONTROL INTERNATIONAL LTD','AVANTI COMMUNICATIONS GROUP PLC','SWIRE COLD STORAGE VIETNAM LTD','FREIGHTLINER GROUP LTD','ELBROOK CASH & CARRY LTD','PHS GROUP HOLDINGS LTD','Fairmont Medical Products Pty Ltd +','James Fisher Technologies LLC','Quintessential Brands UK Holdings Ltd','Befesa Salt Slags Ltd','AVANTI SCREEN MEDIA','Avanti Turkey Uydu Telekomunikasyon LimitedSirketi','International Cleaning Services \(UK\) Ltd','Zenium','Data Merkezi Bir Uretim Sanayi Ve Tic Ltd STI','Peer 1 \(UK\) Ltd +','Magic Seaweed Limited','Pacific Radiology Ltd UK','KCAD Holdings Limited UAE','HBL \(HOLDINGS\) LTD','HBL \(Holdings\) Ltd, H B Leisure Holdings Ltd & h BLeisure Ltd','Maclaren \(HK\) Limited','MACLAREN SERVICES GMBH','Maclaren N.A Inc','MACLAREN JAPAN LIMITED','90 NORTH REAL ESTATE PARTNERS LLP','FERROGLOBE SERVICES UK LTD','ALTERIS CAPITAL PARTNERS LLP','WORLD FUEL SVCS EUROPE LTD','Victus European Student Accommodation 1C Limited','The Goldman Sachs','PZ Cussons','Minerva SA Edible Oils and Food Enterprises','PZ Cussons East Africa Ltd','PZ Cussons Polska S.A.','PZ Cussons \(Thailand\)','PZ CUSSONS MIDDLE EAST & SOUTH ASIA FZE','H B Leisure Malaysia','Orion Project Services Malaysia SDN, BHD +','Orion Engineering Services Nigeria Ltd','OPSA - Pretacao de Servicos, LDA','Orion Project Services PTE Ltd \(Korean Branch\)','Project Services Sakhalin LLC','DEEPOCEAN DE MEXICO S DE R L DE CV','Avanti Communications Tanzania Limited','Avanti Communications Kenya Limited','PALLETWAYS EUROPE LTD','ENERMECH MEXICO S A DE CV','Menzies Aviation \(Hungary\) Kft.','Menzies Aviation Romania SA, Menzies AviationCargo \(Romania\) SRL','Menzies Aviation \(Czech\), s.r.o. +','RSK GROUP PLC','Alexander Dennis Malaysia Sdn Bhd','ALEXANDER DENNIS ASIA PACIFIC LTD','ALEXANDER DENNIS NEW ZEALAND LTD','Alexander Dennis \(Singapore\) Services Pte Limited','SIMS GROUP UK LTD','Petroservicios de Costa Rica S.R.L','Ecuacentair S.A','World Fuel Services \(Japan\) Co. Ltd','Petroservicios De Mexico SA DE CV','Oil Shipping \(Bunkering\) B.V.','World Fuel Services \(Singapore\) Ltd','Mace-Engenharia e Servios, Lda','Mace Australia Propriety Ltd','Mace \(Russia\) Limited','Mace Zagreb d.o.o.','Mace Egypt for Project Management LLC','Mace Ltd \(Hong Kong\)','MACE MANAGEMENT SERVICES LTD','Mace Limited \(Macau\)','Mace d.o.o. Podgorica \(Montenegro\)','Mace Management Services Sarl','Mace Management Services BV','MACE MANAGEMENT SERVICES','Macro Qatar LLC +','Macro Saudi Arabia Ltd','Mace d.o.o \(Serbia\)','Mace Management Services \(Pty\) Ltd','Mace Insaat Ynetim ve Danismanlik Hizmetleri Ltd','MACE INTERNATIONAL LTD','MACE NORTH AMERICA LTD','Mace Vietnam Company Limited','HANSTEEN HOLDINGS PLC','RAMSHOR EUROPE BV','LOVEHONEY GROUP LTD','2XU UK Ltd','2XU UK Limited','Verenigde BedrijvenCentra','FSP COOL LOCKERS UK LTD','STF Power','IDTEC Oman \(International Drilling Technology LLC\)','Lovehoney Limited','EMN BV','RSK Environment LLC','RSK Cevre Hizmetleri AS','RSK Environment WLL  \(UAE\)','Sims Lifecycle Services BV \(The Netherlands\)','SIms me Trade Gmbh \(Austria\)','Sims Lifecycle Services Sp.z.o.o \(Poland\)','Sims Lifecycle Services s.r.o. \(Czech Republic\)','Sims Recycling Solutions FZE \(Dubai\)','BPI X Sarl','THE HALO TRUST COLOMBIA','De La Rue Plc +','Tricel Holdings \(UK\) Limited','Smiggle Ireland Limited','Hansteen Braunschweig Sarl','Hansteen France SAS','Hansteen Netherlands 2 B.V.','Challenger Holdings Vagyonkezelo Kft','SERTEC GROUP HOLDINGS LTD','Gavin de Becker & Associates Asia Pte Ltd','AO World Plc +','ISLESTARR HOLDINGS LTD','Consort Medical PLC','Scitech Engineering Limited','Nord Anglia Education Ltd','ITV PLC','ELEKTRON TECHNOLOGY PLC','De La Rue Lanka Currency & Security Print\(Private\) Ltd','DuPont Authentication inc. Utah','De La Rue Currency & Security Print Ltd','KCA Deutag Drilling \(B\) Sdn Bhd','Sertec','John Cotton Group Ltd +','Avanti Communications South Africa \(Pty\) Limited','TITHEBARN LTD','TEMPO INTERNATIONAL LTD','W H SMITH & SON LTD','MACTAGGART SCOTT USA LLC','Protexin Inc','AB Rhead & Associates Limited','Rhead Group Holdings \(Australia\) Pty Ltd','ITV Studios Australia Pty Limited','Enermech Ltd \(Azerbaijan','PZ Cussons Australia PtyLtd \(Fudge Hair Care Range\)','ST TROPEZ INC','Enermech Petroleum Services','ENERMECH LTD','Enermech WLL \(Kazakhstan','ENERMECH FZE','MENZIES AVIATION COLOMBIA SAS','Menzies Aviation Fuelling Panama Inc.','Aircraft Services International Group','Menzies Aviation St.Maarten B.V','ASIG \(Thailand\) Co Ltd','DEEPOCEAN NORWAY AS','Direct Wines Australia Holdings Pty Ltd+','Aesica Pharmeceuticals GmbH','Aesica Pharmeceuticals srl','Hudson Advisors UK Limited','Charlotte Tilbury Beauty NBV','Pielcolor Uruguay S.A','T.C. USA Inc','Mace Macro Europe Ltd \(Austria\)','Branch Office of Mace International Limited','Mace Macro \(Chile\) SpA','Mace Macro International Investment Limited','Mace Management Services Ltd \(Nigeria\)','Multi Service PTY Limited','WORLD FUEL SERVICES','Mace Macro Europe Ltd \(Czech Republic\)','MACE FINANCE LIMITED','RAMOT I.F.M BEL DOO BEOGRAD','Mace LDA','Mace GmbH','RSK Group Plc \(Budapest Office\)','ITV Studios Inc','Britvic Plc +','Alexander Dennis \(Canada\) Inc','Alexander Dennis Inc.','ITV US Holdings, Inc','Servicio De Contenedores Higienicos Sanitarios S.A','Sice Pty Ltd','Scan Tech AS','Fendercare Marine Sohar LLC','Swire Oilfield Services Norway AS','OMB Valves Inc & Eli Fin Inc','ASIG Nassau Fueling Services \(Bahamas\) Ltd','RSK Environment','RSK Germany GmbH','Davigel','Brake France Service','Davigel \(Spain\)','Cucina Lux Investments Limited +','DL Flange','SIMS M+R GMBH','Sims Recycling Solutions AS','Sims Recycling Solutions AB \(Sweden\)','Rubelli Ltd','PT Orion Indonesia Group','Oemeta Inc','Cos Constantinou','EAVESWAY TRAVEL LTD','North West Ambulance Service','Yorkshire Ambulance Service NHS Trust','East of England Ambulance Service NHS Trust','WEST MIDLANDS AMBULANCE SERVICE','SOUTH CENTRAL AMBULANCE SERVICE NHS FOUNDATION TRUST','Dunn, Mr S L','SOUTH EAST COAST AMBULANCE','NORTH EAST AMBULANCE SVS NHST','East Midlands Ambulance Service NHS Trust','Whitfield, Mr R','FIRSTGROUP PLC','TRANSLINK \(NI\) LTD','ROBERTS CASTINGS LTD','SCOTTISH WATER','LASER TRANSPORT INTERNATIONAL LTD','Amsa Retirement Homes Ltd','Nottingham City Transport Ltd']

class ExtractURLJob(CCSparkJob):
    '''Extracts pages where a list of links are present'''
    name = 'ExtractURLs'

    output_schema = StructType([
        StructField("record_id", StringType(), True),
        StructField("business_name", StringType(), True),
        StructField("date_download", StringType(), True),
        StructField("url", StringType(), True),
        StructField("source_domain", StringType(), True),
        StructField("html", StringType(), True)
        # StructField("title", StringType(), True),
        # StructField("h1", StringType(), True),
        # StructField("description", StringType(), True),
        # StructField("text", StringType(), True)

        # StructField("language", StringType(), True),
        # # StructField("date_modify", StringType(), True),
        # # StructField("date_publish", StringType(), True),
        # StructField("authors", StringType(), True),

        # StructField("image_url", StringType(), True),
        # StructField("localpath", StringType(), True),
        # StructField("title_page", StringType(), True),
        # StructField("title_rss", StringType(), True),
    ])
    def extract_info_from_warc(self, record):
        record_id = record.rec_headers.get_header('WARC-Record-ID')

        date_download = record.rec_headers.get_header('WARC-Date')

        url = record.rec_headers.get_header('WARC-Target-URI')
        
        html = str(record.raw_stream.read())

        ext = tldextract.extract(url)
        source_domain = '.'.join(ext[1:3])

        # # article = Article('')
        # # article.set_html(str(record.raw_stream.read()))
        # # article.parse()

        # soup = BeautifulSoup(str(record.raw_stream.read()))
        # title = soup.title.text
        # h1 = soup.h1.text
        # description_tag = soup.find('meta', {'name': 'description'})
        # if description_tag:
        #     description = description_tag['content']
        # else:
        #     description = ''

        # paragraphs = soup.findAll('p')
        # text = '\n'.join(paragraph.text for paragraph in paragraphs)

        data = {
            'record_id': record_id,
            'date_download': date_download,
            'url': url,
            'source_domain': source_domain,
            'html': html
            # 'title': title,
            # 'h1': h1,
            # 'description': description,
            # 'text': text
        }

        return data


    def process_record(self, record):
        if record.rec_type == 'response':
            # WARC records have three different types:
            #  ["application/warc-fields", "application/http; msgtype=request", "application/http; msgtype=response"]
            self.records_processed.add(1)
            
            data = self.extract_info_from_warc(record)
            HTML = data['html']

            for i, name in enumerate(NAMES):
                if re.search(name, HTML, re.IGNORECASE):
                    yield data['record_id'], name, data['date_download'], data['url'], data['source_domain'], data['html']
        else:
            return

    def run_job(self, sc, sqlc):
        # Redefine to remove reduction step
        input_data = sc.textFile(
            self.args.input, minPartitions=self.args.num_input_partitions)

        output = input_data.mapPartitionsWithIndex(self.process_warcs)

        sqlc.createDataFrame(output, schema=self.output_schema) \
            .coalesce(self.args.num_output_partitions) \
            .write \
            .format("parquet") \
            .saveAsTable(self.args.output)

        self.get_logger(sc).info('records processed = {}'.format(
            self.records_processed.value))

    def run(self):
        self.args = self.parse_arguments()

        conf = SparkConf().setAll((
            ("spark.task.maxFailures", "10"),
            ("spark.locality.wait", "20s"),
            ("spark.serializer", "org.apache.spark.serializer.KryoSerializer"),
            ("spark.dynamicAllocation.enabled", True),
            ("spark.shuffle.service.enabled", True),
        ))
        sc = SparkContext(
            appName=self.name,
            conf=conf)
        sqlc = SQLContext(sparkContext=sc)

        self.records_processed = sc.accumulator(0)
        self.warc_input_processed = sc.accumulator(0)
        self.warc_input_failed = sc.accumulator(0)

        self.run_job(sc, sqlc)

        sc.stop()


if __name__ == "__main__":
    start = time.time()
    job = ExtractURLJob()
    job.run()
    end = time.time()
    print(end - start)

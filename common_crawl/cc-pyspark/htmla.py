import requests
from bs4 import BeautifulSoup, SoupStrainer

response = requests.get('https://www.mirror.co.uk/sport/football/transfer-news/shkodran-mustafi-being-considered-juventus-12885027')
html = response.text

parse_only = SoupStrainer(['h1', 'title', 'meta', 'p'])
soup = BeautifulSoup(html, "lxml", parse_only=parse_only)
print(soup.title.text)
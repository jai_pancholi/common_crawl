from collections import Counter
from mrcc import CCJob

CLIENTS = ['Boris Johnson',
           'Julian Assange',
           'Kate Bush']


class ClientSearch(CCJob):
    def process_record(self, record):
        if record['Content-Type'] != 'text/plain':
            return

        data = record.payload.read()
        for client in CLIENTS:
            if client in data:
                yield client, 1

        self.increment_counter('commoncrawl', 'processed_pages', 1)


if __name__ == '__main__':
    ClientSearch.run()

import re
from collections import Counter
from mrcc import CCJob
from domains import five_thousand_domains, one_hundred_domains
import time

DOMAINS = one_hundred_domains[:5]

class TagCounter(CCJob):
    def process_record(self, record):
        # WARC records have three different types:
        #  ["application/warc-fields", "application/http; msgtype=request", "application/http; msgtype=response"]
        # We're only interested in the HTTP responses
        if record['Content-Type'] == 'application/http; msgtype=response':
            payload = record.payload.read() 
            # The HTTP response is defined by a specification: first part is headers (metadata)
            # and then following two CRLFs (newlines) has the data for the response
            headers, body = payload.split('\r\n\r\n', 1)
            for domain in DOMAINS:
                if domain in body:
                    yield domain, record['WARC-Target-URI']    
                self.increment_counter('commoncrawl', 'processed_pages', 1)


    def reducer(self, key, values):
        """
        The Reduce of MapReduce
        If you're trying to count stuff, this `reducer` will do. If you're
        trying to do something more, you'll likely need to override this.
        """
        yield key, '|'.join(value for value in values)


if __name__ == '__main__':
    start = time.time()
    TagCounter.run()
    end = time.time()
    print(end-start)
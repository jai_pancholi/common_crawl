import gzip
import logging
import os.path as Path

from tempfile import TemporaryFile

import boto3
import botocore
import warc

from mrjob.job import MRJob
from mrjob.util import log_to_stream

from gzipstream import GzipStreamFile


# Set up logging - must ensure that log_to_stream(...) is called only once
# to avoid duplicate log messages (see https://github.com/Yelp/mrjob/issues/1551).
LOG = logging.getLogger('CCJob')
log_to_stream(format="%(asctime)s %(levelname)s %(name)s: %(message)s", name='CCJob')


class CCJob(MRJob):
    """
    A simple way to run MRJob jobs on CommonCrawl Data
    """
    def configure_options(self):
        super(CCJob, self).configure_options()
        self.pass_through_option('--runner')
        self.pass_through_option('-r')
        self.add_passthrough_option('--s3_local_temp_dir',
                                    help='local temporary directory to buffer content from S3',
                                    default=None)

    def process_record(self, record):
        """
        Override process_record with your mapper
        """
        raise NotImplementedError('Process record needs to be customized')

    def mapper(self, _, line):
        """
        The Map of MapReduce
        If you're using Hadoop or EMR, it pulls the CommonCrawl files from S3,
        otherwise it pulls from the local filesystem. Dispatches each file to
        `process_record`.
        """
        # If we're on EC2 or running on a Hadoop cluster, pull files via S3
        if self.options.runner in ['emr', 'hadoop']:
            # Connect to Amazon S3 using anonymous credentials
            boto_config = botocore.client.Config(
                signature_version=botocore.UNSIGNED,
                read_timeout=180,
                retries={'max_attempts' : 20})
            s3client = boto3.client('s3', config=boto_config)
            # Verify bucket
            try:
                s3client.head_bucket(Bucket='commoncrawl')
            except botocore.exceptions.ClientError as exception:
                LOG.error('Failed to access bucket "commoncrawl": %s', exception)
                return
            # Check whether WARC/WAT/WET input exists
            try:
                s3client.head_object(Bucket='commoncrawl',
                                     Key=line)
            except botocore.client.ClientError as exception:
                LOG.error('Input not found: %s', line)
                return
            # Start a connection to one of the WARC/WAT/WET files
            LOG.info('Loading s3://commoncrawl/%s', line)
            try:
                temp = TemporaryFile(mode='w+b',
                                     dir=self.options.s3_local_temp_dir)
                s3client.download_fileobj('commoncrawl', line, temp)
            except botocore.client.ClientError as exception:
                LOG.error('Failed to download %s: %s', line, exception)
                return
            temp.seek(0)
            ccfile = warc.WARCFile(fileobj=(GzipStreamFile(temp)))
        # If we're local, use files on the local file system
        else:
            line = Path.join(Path.abspath(Path.dirname(__file__)), line)
            LOG.info('Loading local file %s', line)
            ccfile = warc.WARCFile(fileobj=gzip.open(line))

        for _i, record in enumerate(ccfile):
            for key, value in self.process_record(record):
                yield key, value
            self.increment_counter('commoncrawl', 'processed_records', 1)

    def combiner(self, key, values):
        """
        Combiner of MapReduce
        Default implementation just calls the reducer which does not make
        it necessary to implement the combiner in a derived class. Only
        if the reducer is not "associative", the combiner needs to be
        overwritten.
        """
        for key_val in self.reducer(key, values):
            yield key_val

    def reducer(self, key, values):
        """
        The Reduce of MapReduce
        If you're trying to count stuff, this `reducer` will do. If you're
        trying to do something more, you'll likely need to override this.
        """
        yield key, sum(values)
import re
from collections import Counter



def get_tag_count(data, ctr=None):
    """Extract the names and total usage count of all the opening HTML tags in the document"""
    if ctr is None:
        ctr = Counter()
    # Convert the document to lower case as HTML tags are case insensitive
    ctr.update(HTML_TAG_PATTERN.findall(data.lower()))
    return ctr


# Optimization: compile the regular expression once so it's not done each time
# The regular expression looks for (1) a tag name using letters (assumes lowercased input) and numbers
# and (2) allows an optional for a space and then extra parameters, eventually ended by a closing >
HTML_TAG_PATTERN = re.compile('<([a-z0-9]+)[^>]*>')
# Let's check to make sure the tag counter works as expected
assert get_tag_count('<html><a href="..."></a><h1 /><br/><p><p></p></p>') == {'html': 1, 'a': 1, 'p': 2, 'h1': 1, 'br': 1}


DOMAINS = [
    'smartology.com',
    'digitalfineprint.com',
    'monzo.com',
    'revolut.com',
    'lemonade.com',
    'carpe.io',
    'coverager.com'
]


class TagCounter(CCJob):
    def process_record(self, record):
        # WARC records have three different types:
        #  ["application/warc-fields", "application/http; msgtype=request", "application/http; msgtype=response"]
        # We're only interested in the HTTP responses
        if record['Content-Type'] == 'application/http; msgtype=response':
            payload = record.payload.read()
            # The HTTP response is defined by a specification: first part is headers (metadata)
            # and then following two CRLFs (newlines) has the data for the response
            headers, body = payload.split('\r\n\r\n', 1)
            for domain in DOMAINS:
                if domain in body:
                    yield domain, record['WARC-Target-URI']    

            # if 'Content-Type: text/html' in headers:


            #     tag_count = get_tag_count(body)
            #     for tag, count in tag_count.items():
            #         yield tag, count
                self.increment_counter('commoncrawl', 'processed_pages', 1)


    def reducer(self, key, values):
        """
        The Reduce of MapReduce
        If you're trying to count stuff, this `reducer` will do. If you're
        trying to do something more, you'll likely need to override this.
        """
        yield key, '|'.join(value for value in values)


if __name__ == '__main__':
    TagCounter.run()
